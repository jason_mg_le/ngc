<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>NGC</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/slider-pro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/owl.carousel.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/tvstyle.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/jquery.sliderPro.js" type="text/javascript"></script>
        <script src="assets/js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="assets/js/tvscript.js" type="text/javascript"></script>
    </head>
    <body class="style2">
        <div class="body">
            <div class="menuleft">
                <ul class="nav flex-column">
                    <li class="nav-item active">
                        <a class="nav-link " href="index.php">
                            <span class="icon">
                                <img src="images/menu-home.png" alt=""/>
                            </span>
                            <span class="text">Home</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#">
                            <span class="icon">
                                <img src="images/menu-help.png" alt=""/>
                            </span>
                            <span class="text">Help</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#">
                            <span class="icon">
                                <img src="images/menu-star.png" alt=""/>
                            </span>
                            <span class="text">Star</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="latest-news.html">
                            <span class="icon">
                                <img src="images/menu-latest.png" alt=""/>
                            </span>
                            <span class="text">Latest news</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="events.html">
                            <span class="icon">
                                <img src="images/menu-events.png" alt=""/>
                            </span>
                            <span class="text">Events</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="announcement.html">
                            <span class="icon">
                                <img src="images/menu-forma.png" alt=""/>
                            </span>
                            <span class="text">Announcement</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="calendar.html">
                            <span class="icon">
                                <img src="images/menu-calendar.png" alt=""/>
                            </span>
                            <span class="text">Calander</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#">
                            <span class="icon">
                                <img src="images/menu-quicklinks.png" alt=""/>
                            </span>
                            <span class="text">Quick links</span>
                        </a>
                        <div class="menu-dropdown">
                            <ul class="nav flex-column">
                                <li>
                                    <a href="department.html">
                                        <span>&nbsp;</span>
                                        Company Structure
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>&nbsp;</span>
                                        Employee List
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>&nbsp;</span>
                                        Departments Report
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>&nbsp;</span>
                                        Other Intranet link 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>&nbsp;</span>
                                        Other Intranet link 2
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>&nbsp;</span>
                                        Other Intranet link 3
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="index.php">
                                <img src="images/logo.png" alt=""/>
                            </a>
                        </div>
                        <div class="col-md-5">
                            <div class="input-group form-search">
                                <input class="form-control input-search">
                                <div class="input-group-prepend">
                                    <span class="text-center">
                                        <img src="images/icon-search.png" alt=""/>
                                        <br>
                                        SEARCH
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <ul class="nav menutop">
                                <li class="nav-item menubg1">
                                    <a class="nav-link active" href="intranet_profile.html">
                                        <img src="images/icon-user.png" alt=""/>
                                        <br/>
                                        MY PROFILE
                                    </a>
                                </li>
                                <li class="nav-item menubg2">
                                    <a class="nav-link" href="inbox.html">
                                        <img src="images/icon-inbox.png" alt=""/><br/>
                                        INBOX</a>
                                </li>
                                <li class="nav-item menubg3">
                                    <a class="nav-link" href="#">
                                        <img src="images/icon-logout.png" alt=""/><br/>
                                        LOGOUT</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <section class="tvmain">
                <div class="form-group clearfix">&nbsp;</div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="moduletable">

                                <!-- Style Block -->
                                <style type="text/css">
                                    #vina-slideshow-content526 {
                                        margin: 0px 0px 0px 0px;
                                        padding: 0px 0px 0px 0px;
                                    }
                                    #vina-slideshow-content526 .news-caption {
                                        color: #ffffff;
                                    }
                                    #vina-slideshow-content526 .news-caption a {
                                        color: #f44336;
                                    }
                                    #vina-slideshow-content526 .news-category,
                                    #vina-slideshow-content526 .news-category a {
                                        background-color: #01978c;
                                        color: #ffffff !important;
                                    }
                                    .sp-thumbnail-container {
                                        background-color: #f4f6f7;
                                    }
                                    #vina-slideshow-content526 .sp-thumbnail-image-container {
                                        height: 77px;
                                        width: 115px;
                                    }
                                    #vina-slideshow-content526 .sp-thumbnail-text {
                                        width: 100%;
                                    }
                                </style>


                                <!-- HTML Block -->
                                <div class="vina-slideshow-content slider-pro" id="vina-slideshow-content526">
                                    <!-- Main Slide Item Block -->
                                    <div class="sp-slides">
                                        <div class="sp-slide">
                                            <img data-src="images/image-11.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 03" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                        <div class="sp-slide">
                                            <img data-src="images/image-12.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 04" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                        <div class="sp-slide">
                                            <img data-src="images/image-13.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 05" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                        <div class="sp-slide">
                                            <img data-src="images/image-14.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 06" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                        <div class="sp-slide">
                                            <img data-src="images/image-2.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 07" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                        <div class="sp-slide">
                                            <img data-src="images/image-4.jpg" src="images/blank.gif" alt=" H3 Tag - This is title of article 08" class="sp-image">	

                                            <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> What's New</a></div>

                                        </div>
                                    </div>

                                    <!-- Thumbnails Block -->
                                    <div class="sp-thumbnails">
                                        <div class="sp-thumbnail">
                                            <div class="sp-thumbnail-text hide-small-screen">
                                                <div class="sp-thumbnail-title"> LPG Cylinders</div>
                                                <div class="sp-thumbnail-description">LPG is the preferred fuel for applications that..</div>
                                            </div>
                                        </div>
                                        <div class="sp-thumbnail">
                                            <div class="sp-thumbnail-text hide-small-screen">
                                                <div class="sp-thumbnail-title"> NGC Energy Edge</div>
                                                <div class="sp-thumbnail-description">LPG is the preferred fuel for applications that..</div>
                                            </div>
                                        </div>
                                        <div class="sp-thumbnail">
                                            <div class="sp-thumbnail-text hide-small-screen">
                                                <div class="sp-thumbnail-title"> LPG Cylinders</div>
                                                <div class="sp-thumbnail-description">LPG is the preferred fuel for applications that..</div>
                                            </div>
                                        </div>
                                        <div class="sp-thumbnail">
                                            <div class="sp-thumbnail-text hide-small-screen">
                                                <div class="sp-thumbnail-title"> LPG Cylinders</div>
                                                <div class="sp-thumbnail-description">LPG is the preferred fuel for applications that..</div>
                                            </div>
                                        </div>
                                        <div class="sp-thumbnail">
                                            <div class="sp-thumbnail-text hide-small-screen">
                                                <div class="sp-thumbnail-title"> NGC Energy Edge</div>
                                                <div class="sp-thumbnail-description">LPG is the preferred fuel for applications that..</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- Javascript Block -->
                                <script type="text/javascript">
                                    jQuery(document).ready(function ($) {
                                        $('#vina-slideshow-content526').sliderPro({
                                            width: 485,
                                            height: 300,
                                            startSlide: 0,
                                            shuffle: true,
                                            orientation: 'horizontal',
                                            forceSize: 'none',
                                            loop: true,
                                            slideDistance: 0,
                                            slideAnimationDuration: 700,
                                            heightAnimationDuration: 700,
                                            fade: false,
                                            fadeOutPreviousSlide: true,
                                            fadeDuration: 500,
                                            autoplay: false,
                                            autoplayDelay: 5000,
                                            autoplayOnHover: 'pause',
                                            arrows: true,
                                            fadeArrows: true,
                                            touchSwipe: false,
                                            fadeCaption: true,
                                            captionFadeDuration: 500,
                                            thumbnailWidth: 250,
                                            thumbnailHeight: 97,
                                            thumbnailsPosition: 'right',
                                            thumbnailPointer: true,
                                            thumbnailArrows: true,
                                            buttons: false,
                                            autoScaleLayers: false,
                                            breakpoints: {
                                                800: {
                                                    thumbnailsPosition: 'bottom',
                                                    thumbnailWidth: 220,
                                                    thumbnailHeight: 95
                                                },
                                                500: {
                                                    thumbnailsPosition: 'bottom',
                                                    thumbnailWidth: 120,
                                                    thumbnailHeight: 95
                                                }}
                                        });
                                    });
                                </script>
                            </div>
                            <div class="moduletable tv-banner">
                                <div class="form-group clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6 pr0 bgblueaf">
                                        <div class="col-md-12">
                                            <h1>Need To Know</h1>
                                            <p>NGC Energy as an energy company is committed to enhancing the quality of life by providing energy solutions that help fuel and fulfill the aspirations of consumers everywhere.</p>
                                            <a href="#" class="pull-right">
                                                <img src="images/icon-right.png" alt=""/></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl0 pl0">
                                        <img class=" img100" src="images/img1.jpg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="moduletable">
                                <div class="form-group clearfix"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="box-slide bgwhite ">
                                            <div class="box-title">
                                                <h1><img src="images/icon-latest.png" alt=""/> <span>Latest News</span></h1>
                                            </div>
                                            <div class="box-content pl20">
                                                <div class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest1.jpg" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Web Award</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest3.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Mark in SNG</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC Energy Edge</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest1.jpg" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Web Award</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest3.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Mark in SNG</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC Energy Edge</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest1.jpg" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Web Award</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest3.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC's Mark in SNG</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">NGC Energy Edge</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/latest5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="">Mira Gas in Malaysia</a></h2>
                                                                        <p class="text-fade">
                                                                            Posted on 20/02/2017   
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <a class="go-laltest" href="latest-news.html">&nbsp;</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box-slide bgwhite">
                                            <div class="box-title">
                                                <h1><img src="images/icon-event.png" alt=""/> <span>Event</span></h1>
                                            </div>
                                            <div class="box-content pl20">
                                                <div class="owl-carousel owl-theme">
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event1.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Annual Meeting 2017</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Red Carpet</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/noimage.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Business Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Marketing Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">NGC Lunch Meeting</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event1.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Annual Meeting 2017</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Red Carpet</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/noimage.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Business Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Marketing Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">NGC Lunch Meeting</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="item">
                                                        <ul class="nav flex-column">
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event1.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Annual Meeting 2017</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event2.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Red Carpet</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/noimage.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Business Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event4.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">Marketing Workshop</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="images/event5.png" alt=""/>
                                                                    </div>    
                                                                    <div class="col-9">
                                                                        <h2><a href="#">NGC Lunch Meeting</a></h2>
                                                                        <p class="text-fade">
                                                                            <img src="images/icon-calender.png" alt=""/>03/03/2016
                                                                            <img src="images/icon-clock.png" alt=""/>08.00pm
                                                                        </p>
                                                                    </div>    
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <a class="go-events" href="events.html">&nbsp;</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-right">
                            <div class="box-right bgwhite">
                                <div class="box-title text-center">
                                    <p class="last_login">Last login : 22 Feb 2017 10:20:31am</p>
                                    <p class="data_today">
                                        <span class="font25 bold disblock">Friday</span>
                                        <span class="font60 bold disblock">26</span>
                                        <span class="font25 bold disblock">12:20pm</span>
                                    </p>
                                </div>
                                <div class="box-calendar bgblue">
                                    <div class="box-content ">
                                        <?php include_once 'loadcalendar2.php'; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix"></div>
                            <div class="box-right bgwhite announcement announcement2">
                                <div class="box-title brnone">
                                    <h1>Announcement</h1>
                                </div>
                                <div class="box-content">
                                    <div class="row">
                                        <div class="col-1 text-right">
                                            1
                                        </div>    
                                        <div class="col-10">
                                            Annual Dinner Reservation is now open for register.
                                        </div>     
                                    </div>
                                    <div class="row">
                                        <div class="col-1 text-right">
                                            2
                                        </div>    
                                        <div class="col-10">
                                            For an organization to achieve brilliance and to set new standards.
                                        </div>      
                                    </div>
                                    <a class="go-announcement" href="events.html">&nbsp;</a>
                                </div>
                            </div>
                            <div class="form-group clearfix"></div>
                            <div class="box-right bgwhite quick_links">
                                <div class="box-title brnone">
                                    <h1>Quick Links</h1>
                                </div>
                                <div class="box-content">
                                    <ul class="nav flex-column">
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Company Structure
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Employee List
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Departments Report
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Other Intranet link 1
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Other Intranet link 2
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="noicon">&nbsp;</span>
                                                Other Intranet link 3
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group clearfix">&nbsp;</div>
                <div class="form-group clearfix">&nbsp;</div>
            </section>
            <footer>
                <div class="container">
                    <div class="row">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link">© NGC Energy Berhad (435318-U). All rights reserved.</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Terms & Conditions   </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy</a>
                            </li>
                        </ul>
                    </div>
                    <script type="text/javascript">function add_chatinline() {
                            var hccid = 41571659;
                            var nt = document.createElement("script");
                            nt.async = true;
                            nt.src = "https://mylivechat.com/chatinline.aspx?hccid=" + hccid;
                            var ct = document.getElementsByTagName("script")[0];
                            ct.parentNode.insertBefore(nt, ct);
                        }
                        add_chatinline();</script>
                </div>
            </footer>
        </div>
    </body>
</html>
