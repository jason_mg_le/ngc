<style type="text/css">
    #calendar_99 table
    {
        border-collapse: initial;
        border:0px;
    }

    #calendar_99 table td
    {
        padding: 0px;
        vertical-align: none;
        border-top:none;
        line-height: none;
        text-align: none;

    }

    #calendar_99 .cell_body td
    {
        border:1px solid #D2D2D2;
    }


    #calendar_99 p, ol, ul, dl, address
    {
        margin-bottom:0;

    }


    #calendar_99 td,#calendar_99 tr,  #spiderCalendarTitlesList_99 td,  #spiderCalendarTitlesList_99 tr

    {
        border:none;
    }


    #calendar_99 .cala_arrow a:link, #calendar_99 .cala_arrow a:visited {
        color:#CED1D0;
        text-decoration:none !important;
        background:none;
        font-size:16px;
    }
    #calendar_99 .cala_arrow a:hover {
        color:#CED1D0;

        text-decoration:none;
        background:none;
    }
    #calendar_99 .cala_day a:link, #calendar_99 .cala_day a:visited {
        text-decoration:none !important;
        background:none;
    }
    #calendar_99 .cala_day a:hover {
        text-decoration:none !important;
        background:none;
    }
    #calendar_99 .calyear_table {
        border-spacing:0;
        width:100%;
    }
    #calendar_99 .calmonth_table {	
        border-spacing:0;
        width:100%;
    }
    #calendar_99 .calbg
    {

        text-align:center;
    }
    #calendar_99 .caltext_color_other_months 
    {
        color:#939699;
    }
    #calendar_99 .caltext_color_this_month_unevented {
        color:#989898;
    }
    #calendar_99 .calfont_year {
        font-size:24px;
        font-weight:bold;
        color:#000;
    }

    #calendar_99 .calsun_days 
    {
        color:#989898;
    }


    #calendar_99 .calborder_day
    {
        border: solid #000 1px;
    }

    #spiderCalendarTitlesList_99
    {
        display:none; width:331px; margin:0px; padding:0px; border:none; z-index:99;position:fixed;  color:#000000;
    }

    #spiderCalendarTitlesList_99 #sc1 
    {
        padding:0px; margin:0px; height:65px; background:url('/joomla/Joomla_38/modules/mod_spidercalendar/images
            /TitleListBg1.png') no-repeat;
    }
    #spiderCalendarTitlesList_99 #sc2
    {
        padding:0px; margin:0px; background:url('/joomla/Joomla_38/modules/mod_spidercalendar/images/TitleListBg2
            .png') repeat-y;

    }
    #spiderCalendarTitlesList_99 #sc3
    {
        padding:0px; margin:0px; height:32px; background:url('/joomla/Joomla_38/modules/mod_spidercalendar/images
            /TitleListBg3.png') no-repeat;
    }
    #spiderCalendarTitlesList_99 p
    {
        margin:20px;
        margin-top:0px;
        text-align:left;
    }
    .categories p:last-child:first-letter
    {
        color:#fff;
    }

    .categories p:last-child
    {
        position:relative;
        left:-9px;
    }
    .categories p
    {
        display:inline-block;
        cursor:pointer;
    }


</style>
<table cellpadding="0" cellspacing="0"  style="border-spacing:0; width:200 px; height:190px; border:#000 solid 0px; margin:0; padding:0;">

    <tr>

        <td width="100%" style=" padding:0; margin:0">

            <form action="" method="get" style="background:none; margin:0; padding:0;">

                <table cellpadding="0" cellspacing="0" border="0" style="border-spacing:0; 
                       ; margin:0; padding:0;" width="200" height="190">



                    <tr  height="28px" style="width:200px">

                        <td class="calbg" colspan="7" style="background-image: url('/joomla/Joomla_38/components
                            /com_spidercalendar/views/bigcalendar/images/Stver.png');margin:0; padding:0;background-repeat: no-repeat
                            ;background-size: 100% 100%;" >


                            <table cellpadding="0" cellspacing="0" border="0" align="center" class="calmonth_table"
                                   style="width:100%; margin:0; padding:0">

                                <tr>

                                    <td style="text-align:left; margin:0; padding:0; line-height:16px" class
                                        ="cala_arrow cala_pre" width="20%"><a  
                                            href="javascript:showcalendar( 'calendar_99','/joomla/Joomla_38/index
                                            .php/component/spidercalendar/?view=AJAXcalendar&calendar=1&module_id=99&cat_id=&cat_ids
                                            =&format=row&Itemid=101&date99=2017-12')">&nbsp;</a></td>

                                    <td width="60%" style="text-align:center; margin:0; padding:0" >

                                        <input type="hidden" name="month" readonly="" value="January"/>

                                        <h4>January </h4></td>

                                    <td style="text-align:right; margin:0; padding:0; line-height:16px" 
                                        class="cala_arrow cala_next" width="20%"><a href="javascript:showcalendar( 'calendar_99','/joomla/Joomla_38/index
                                                                                .php/component/spidercalendar/?view=AJAXcalendar&calendar=1&module_id=99&cat_id=&cat_ids
                                                                                =&format=row&Itemid=101&date99=2018-02')">&nbsp;</a></td>

                                </tr>

                            </table>

                        </td>

                    </tr>

                    <tr class="cell_body" align="center"  height="10%" style="width
                        :200px">


                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding
                            :0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> M </b></div></td>

                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding
                            :0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> T </b></div></td>

                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding
                            :0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> W </b></div></td>

                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding
                            :0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> T </b></div></td>

                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding:0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> F </b></div></td>

                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0; padding
                            :0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> S </b></div></td>


                        <td style="width:28.571428571429px; font-family:;	color:#2F647D; margin:0;
                            padding:0">

                            <div class="calbottom_border" style="text-align:center; width:28.571428571429px
                                 ; margin:0; padding:0;"><b> S </b></div></td>
                    </tr>

                    <tr class="cell_body" height="21.4px" style="font-family:;line-height:21.4px"><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>01</b></td><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>02</b></td><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>03</b></td><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>04</b></td><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>05</b></td><td
                            style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>06</b></td><td
                            class="calsun_days" style="text-align:center;padding:0; margin:0;line-height:inherit;"><b>07</b></td
                        ></tr><tr class="cell_body" height="21.4px" style="font-family:;line-height:21.4px"><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>08</b></td><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>09</b></td><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>10</b></td><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>11</b></td><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>12</b></td><td style="text-align
                                                                                                            :center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>13</b></td><td class="calsun_days"
                                                                                                            style="text-align:center;padding:0; margin:0;line-height:inherit;"><b>14</b></td></tr><tr class="cell_body"
                                                                                                              height="21.4px" style="font-family:;line-height:21.4px"><td style="text-align:center; color:#989898
                                                                                ;padding:0; margin:0; line-height:inherit;"><b>15</b></td><td style="text-align:center; color:#989898
                                                                                ;padding:0; margin:0; line-height:inherit;"><b>16</b></td><td style="text-align:center; color:#989898
                                                                                ;padding:0; margin:0; line-height:inherit;"><b>17</b></td><td style="text-align:center; color:#989898
                                                                                ;padding:0; margin:0; line-height:inherit;"><b>18</b></td><td style="text-align:center; color:#989898
                                                                                ;padding:0; margin:0; line-height:inherit; border: 2px solid #"><b>19</b></td><td style="text-align:center
                                                                                ; color:#989898;padding:0; margin:0; line-height:inherit;"><b>20</b></td><td class="calsun_days" style
                                                                                ="text-align:center;padding:0; margin:0;line-height:inherit;"><b>21</b></td></tr><tr class="cell_body"
                                                                                                         height="21.4px" style="font-family:;line-height:21.4px">
                        <td class="cala_day" style="text-align:center;padding:0; margin:0;line-height
                            :inherit;"><a  class="calan_tooltip" data-tooltip-content="#tooltip_content1" ><b>22</b></a>
                            <div class="tooltip_templates">
                                <span id="tooltip_content1">
                                    <span class="tooltip-titile">Annual Dinner</span>
                                    <span class="tooltip-time">08:00pm</span>
                                </span>
                            </div>
                        </td>
                        <td style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit;"><b>23</b
                            >
                        </td>
                        <td class="cala_day" style="text-align:center;padding:0; margin:0;line-height
                            :inherit;"><a  class="calan_tooltip" data-tooltip-content="#tooltip_content2" ><b>24</b></a>
                            <div class="tooltip_templates">
                                <span id="tooltip_content2">
                                    <span class="tooltip-titile">Annual Dinner</span>
                                    <span class="tooltip-time">08:00pm</span>
                                </span>
                            </div>
                        </td>
                        <td style="text-align:center; color:#989898;padding
                            :0; margin:0; line-height:inherit;"><b>25</b></td><td style="text-align:center; color:#989898;padding
                            :0; margin:0; line-height:inherit;"><b>26</b></td><td style="text-align:center; color:#989898;padding
                            :0; margin:0; line-height:inherit;"><b>27</b></td><td class="calsun_days" style="text-align:center;padding
                            :0; margin:0;line-height:inherit;"><b>28</b></td></tr><tr class="cell_body" height="21.4px" style="font-family
                                             :;line-height:21.4px"><td style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit
                          ;"><b>29</b></td><td style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit
                          ;"><b>30</b></td><td style="text-align:center; color:#989898;padding:0; margin:0; line-height:inherit
                          ;"><b>31</b></td><td class="caltext_color_other_months" style="text-align:center;"></td><td class="caltext_color_other_months"
                          style="text-align:center;"></td><td class="caltext_color_other_months" style="text-align:center;">
                        </td><td class="caltext_color_other_months" style="text-align:center;"></td></tr>


                </table>




                <input type="text" value="1" name="day" style="display:none" />

            </form>



        </td>

    </tr>

</table>
<link href="assets/css/tooltipster.main.css" rel="stylesheet" type="text/css"/>
<script src="assets/js/tooltipster.bundle.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.calan_tooltip').tooltipster({
            animation: 'fade',
//            delay: 200,
//            theme: 'tooltipster-punk',
//            trigger: 'click'
        });
    });
</script>
