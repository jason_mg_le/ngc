
/*
 * Created on : Jan 19, 2018, 11:38:47 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    var owlCarousel = $('.owl-carousel');
    if (owlCarousel.length) {
        owlCarousel.owlCarousel({
            items: 1,
            margin: 10,
            nav: false,
            loop: true
        })
    }
    $('.all-mess').click(function (e) {
        e.preventDefault();
        updateContent('.list-inbox', 'all-mess.html')
    })
    $('.unread-mess').click(function (e) {
        e.preventDefault();
        updateContent('.list-inbox', 'unread.html')
    })
    $('.newest-mess').click(function (e) {
        e.preventDefault();
        updateContent('.list-inbox', 'newest.html')
    })
    $('.box-mess > h3').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.box-mess');
        if ($parent.is('.unread')) {
            $parent.removeClass('unread');
            $parent.addClass('read-more');
            $parent.find('.content-mess').show();
            /*
             * Chay ajax cap nhat trang thai da doc
             */
        } else {
            if ($parent.is('.read-more')) {
                $parent.removeClass('unread');
                $parent.removeClass('read-more');
                $parent.find('.content-mess').hide();
            } else {
                $parent.removeClass('unread');
                $parent.addClass('read-more');
                $parent.find('.content-mess').show();
            }
        }
    })

    function updateContent($eleupdate, $url) {
        $(".body").LoadingOverlay("show");
        $.ajax({
            url: $url,
            success: function (data, textStatus, jqXHR) {
                $(".body").LoadingOverlay("hide");
                $($eleupdate).html(data)
            }
        })
    }

    $('ul.nav.tv-tab .btn-group').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
})