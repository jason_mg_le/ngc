<?php

/**
 * Description of Helper
 *
 * @author Le Xuan Chien (chien.lexuan@gmail.com)
 */
defined('_JEXEC') or die('Restricted access');

/* @var $docment JDocumentHTML */

// Include the syndicate functions only once

$showAll = $params->get('showAllChildren');
$class_sfx = htmlspecialchars($params->get('class_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_tvuser', $params->get('layout', 'default'));
?>