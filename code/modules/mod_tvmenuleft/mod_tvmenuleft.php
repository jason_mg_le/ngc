<?php

/**
 * Description of Helper
 *
 * @author Le Xuan Chien (chien.lexuan@gmail.com)
 */
defined('_JEXEC') or die('Restricted access');

/* @var $docment JDocumentHTML */

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$helper = new ModMenuLeftHelper();
$list = $helper->getList($params);
$base = $helper->getBase($params);
$active = $helper->getActive($params);
$default = $helper->getDefault();
$active_id = $active->id;
$default_id = $default->id;
$path = $base->tree;
$showAll = $params->get('showAllChildren');
$class_sfx = htmlspecialchars($params->get('class_sfx'), ENT_COMPAT, 'UTF-8');


if (count($list)) {
    require JModuleHelper::getLayoutPath('mod_tvmenuleft', $params->get('layout', 'default'));
}
?>