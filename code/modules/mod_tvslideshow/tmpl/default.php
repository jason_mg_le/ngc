<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>

<div class="moduletable <?php echo $moduleclass_sfx; ?>">

    <!-- Style Block -->
    <style type="text/css">
        #vina-slideshow-content<?php echo $module->id ?> {
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
        }
        #vina-slideshow-content<?php echo $module->id ?> .news-caption {
            color: #ffffff;
        }
        #vina-slideshow-content<?php echo $module->id ?> .news-caption a {
            color: #f44336;
        }
        #vina-slideshow-content<?php echo $module->id ?> .news-category,
        #vina-slideshow-content<?php echo $module->id ?> .news-category a {
            background-color: #01978c;
            color: #ffffff !important;
        }
        .sp-thumbnail-container {
            background-color: #f4f6f7;
        }
        #vina-slideshow-content<?php echo $module->id ?> .sp-thumbnail-image-container {
            height: 77px;
            width: 115px;
        }
        #vina-slideshow-content<?php echo $module->id ?> .sp-thumbnail-text {
            width: 100%;
        }
    </style>


    <!-- HTML Block -->
    <div class="vina-slideshow-content slider-pro" id="vina-slideshow-content<?php echo $module->id ?>">
        <!-- Main Slide Item Block -->
        <div class="sp-slides">
            <?php
            foreach ($list as $item) :
                $image = json_decode($item->images);
                ?>
                <div class="sp-slide" itemscope itemtype="https://schema.org/Article">
                    <a href="<?php echo $item->link; ?>" itemprop="url">
                        <img data-src="<?php echo JUri::base() . $image->image_intro ?>" src="images/blank.gif" alt="<?php echo $item->title; ?>" class="sp-image">	
                    </a>
                    <div data-position="topLeft" class="sp-layer sp-padding news-category"><a href="#"><i class="fa fa-star" aria-hidden="true"></i> <?php echo $module->title ?></a></div>
                </div>
            <?php endforeach; ?>
        </div>

        <!-- Thumbnails Block -->
        <div class="sp-thumbnails">
            <?php foreach ($list as $item) : ?>
                <div class="sp-thumbnail">
                    <div class="sp-thumbnail-text hide-small-screen">
                        <div class="sp-thumbnail-title"> <?php echo $item->title; ?></div>
                        <div class="sp-thumbnail-description"><?php echo $item->introtext; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>


    <!-- Javascript Block -->
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#vina-slideshow-content<?php echo $module->id ?>').sliderPro({
                width: 485,
                height: 300,
                startSlide: 0,
                shuffle: true,
                orientation: 'horizontal',
                forceSize: 'none',
                loop: true,
                slideDistance: 0,
                slideAnimationDuration: 700,
                heightAnimationDuration: 700,
                fade: false,
                fadeOutPreviousSlide: true,
                fadeDuration: 500,
                autoplay: false,
                autoplayDelay: 5000,
                autoplayOnHover: 'pause',
                arrows: true,
                fadeArrows: true,
                touchSwipe: false,
                fadeCaption: true,
                captionFadeDuration: 500,
                thumbnailWidth: 250,
                thumbnailHeight: 97,
                thumbnailsPosition: 'right',
                thumbnailPointer: true,
                thumbnailArrows: true,
                buttons: false,
                autoScaleLayers: false,
                breakpoints: {
                    800: {
                        thumbnailsPosition: 'bottom',
                        thumbnailWidth: 220,
                        thumbnailHeight: 95
                    },
                    500: {
                        thumbnailsPosition: 'bottom',
                        thumbnailWidth: 120,
                        thumbnailHeight: 95
                    }}
            });
        });
    </script>
</div>