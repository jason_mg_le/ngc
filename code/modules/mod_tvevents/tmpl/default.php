<?php
/*
 * Created on : Jan 24, 2018, 9:30:18 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
?>
<div class="box-slide bgwhite <?php echo $moduleclass_sfx; ?>">
    <div class="box-title">
        <h1><img src="images/icon-latest.png" alt=""/> <span><?php echo $module->title; ?></span></h1>
    </div>
    <div class="box-content pl20">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <ul class="nav flex-column">
                    <?php
                    $c = count($list);
                    $r = ceil($c / 3);
                    $r = 5;
                    $i = 1;
                    if (count($c) > 0) {
                        foreach ($list as $key => $value) {
                            ?>
                            <li>
                                <div class="row">
                                    <div class="col-3">
                                        <?php foreach ((array) $value->images as $fileSingle) :
                                            ?>
                                            <?php if (!is_array($fileSingle) && $fileSingle != '') : ?>
                                                <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $value->id); ?>">
                                                    <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . 'uploads/' . $fileSingle . '&w=59&h=57&q=85'; ?>" width="309"/>
                                                </a> 
                                            <?php else: ?>
                                                <a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $value->id); ?>">
                                                    <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . 'components/com_events/assets/images/noimageevent.png&w=59&h=57&q=85'; ?>" width="309"/>
                                                </a> 
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>    
                                    <div class="col-9">
                                        <h2><a href="<?php echo JRoute::_('index.php?option=com_events&view=event&id=' . (int) $value->id); ?>"><?php echo $value->title ?></a></h2>
                                        <?php
                                        if (!$value->time) {
                                            $from[0] = "";
                                            $from[1] = "";
                                            $to[0] = "";
                                            $to[1] = "";
                                        } else {
                                            $from_to = explode("-", $value->time);
                                            $from = explode(":", $from_to[0]);
                                            if (isset($from_to[1]))
                                                $to = explode(":", $from_to[1]);
                                            else {
                                                $to[0] = "";
                                                $to[1] = "";
                                            }
                                        }
                                        ?> 
                                        <p class="text-fade">
                                            <img src="images/icon-calender.png" alt=""/><?php echo $helper->formatDate($value->date, 'd/m/Y'); ?>
                                            <img src="images/icon-clock.png" alt=""/><span class="text-lowercase"><?php echo $from_to[0]; ?></span>
                                        </p>
                                    </div>    
                                </div>
                            </li>
                            <?php
                            if ($i % $r == 0 && $i < $c) {
                                ?>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="nav flex-column">
                                <?php
                            }
                            $i++;
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        <a class="go-events" href="<?php echo JRoute::_('index.php?option=com_events&view=events'); ?>">&nbsp;</a>
    </div>
</div>