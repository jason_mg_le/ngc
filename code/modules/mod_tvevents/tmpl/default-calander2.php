<?php
/*
 * Created on : Jan 24, 2018, 9:30:18 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_events/assets/css/calendar.css');
$document->addStyleSheet(JUri::root() . 'components/com_events/assets/css/tooltipster.main.css');
$document->addScript(JUri::root() . 'components/com_events/assets/js/tooltipster.bundle.min.js');
$document->addScript(JUri::root() . 'components/com_events/assets/js/loadingoverlay.js');

function Month_num($month_name) {
    for ($month_num = 1; $month_num <= 12; $month_num++) {
        if (date("F", mktime(0, 0, 0, $month_num, 1, 0)) == $month_name and ( $month_num < 10)) {
            return '0' . $month_num;
        } else {
            return $month_num;
        }
    }
}

$date = JRequest::getVar("date", date("Y") . '-' . Month_num(date("F")) . '-' . date("d"));
$year = substr($date, 0, 4);
$month = substr($date, 5, 2);
$user = JFactory::getUser();
?>
<div class="box-right bgwhite">
    <div class="box-title text-center">
        <?php if ($user->id > 0) { ?>
            <p class="last_login">Last login : 
                <?php
                echo $helper->formatDate($user->get('lastvisitDate'), 'd/m/Y H:i:s a');
                ?>
            </p>
        <?php }else{
            ?>
            <p style="height: 1px;"></p>
            <?php
        } ?>
        <p class="data_today">
            <span class="font25 bold disblock"><?php echo date("l");?></span>
            <span class="font60 bold disblock"><?php echo date("j");?></span>
            <span class="font25 bold disblock"><?php echo date("H:i a");?></span>
        </p>
    </div>
    <div class="box-calendar bgblue">
        <div class="box-content ">

        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function ($) {
        $.ajax({
            url: 'index.php?option=com_events&view=ajaxcalendar&format=raw&date=' + "<?php echo $year; ?>" + '-' + "<?php echo $month; ?>",
            type: 'post',
            dataType: 'html',
            success: function (data, textStatus, jqXHR) {
                $(".box-calendar .box-content").html(data);
                $('.calan_tooltip').tooltipster({
                    animation: 'fade',
//            delay: 200,
//            theme: 'tooltipster-punk',
//            trigger: 'click'
                });
            }
        })
        $(document).on('click', '#calendar .prev', function (e) {
            e.preventDefault();
            $(".box-calendar").LoadingOverlay("show");
            var $url = $(this).attr('href');
            $.ajax({
                url: $url,
                type: 'post',
                dataType: 'html',
                success: function (data, textStatus, jqXHR) {
                    $(".box-calendar .box-content").html(data);
                    $('.calan_tooltip').tooltipster({
                        animation: 'fade'
                    });
                    $(".box-calendar").LoadingOverlay("hide");
                }
            })
        })
        $(document).on('click', '#calendar .next', function (e) {
            e.preventDefault();
            $(".box-calendar").LoadingOverlay("show");
            var $url = $(this).attr('href');
            $.ajax({
                url: $url,
                type: 'post',
                dataType: 'html',
                success: function (data, textStatus, jqXHR) {
                    $(".box-calendar .box-content").html(data);
                    $('.calan_tooltip').tooltipster({
                        animation: 'fade'
                    });
                    $(".box-calendar").LoadingOverlay("hide");
                }
            })
        })
    })
</script>