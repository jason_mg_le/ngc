<?php

/**
 * Description of Helper
 *
 * @author Tran Trong Thang (trantrongthang1207@gmail.com)
 */
defined('_JEXEC') or die('Restricted access');

/* @var $docment JDocumentHTML */
// Include the menu functions only once
JLoader::register('ModTvEventsHelper', __DIR__ . '/helper.php');

$helper = new ModTvEventsHelper();

if ($params->get('layout', 'default') == '_:default-calander') {
    $list = $helper->getList($params);
} else {
    $list = $helper->getList($params);
}
$wlimit = $params->get('wordlimit', 100);
$end_char = $params->get('end_char', '...');
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

if (count($list)) {
    require JModuleHelper::getLayoutPath('mod_tvevents', $params->get('layout', 'default'));
}
?>