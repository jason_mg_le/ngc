<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_job79/models', 'Job79Model');

use Joomla\Utilities\ArrayHelper;

/**
 * Helper for mod_menu
 *
 * @package     Joomla.Site
 * @subpackage  mod_menu
 * @since       1.5
 */
class ModTvEventsHelper {

    /**
     * Get a list of the menu items.
     *
     * @param   \Joomla\Registry\Registry  &$params  The module options.
     *
     * @return  array
     *
     * @since   1.5
     */
    public static function getList(&$params) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*');

        $query->from('`#__events_event` AS a');

        $case_when = ' CASE WHEN ';
        $case_when .= $query->charLength('a.alias', '!=', '0');
        $case_when .= ' THEN ';
        $a_id = $query->castAsChar('a.id');
        $case_when .= $query->concatenate(array($a_id, 'a.alias'), '-');
        $case_when .= ' ELSE ';
        $case_when .= $a_id . ' END as slug';
        $query->select($case_when);

        $query->where('state = 1');

// Set ordering
        $order_map = array(
            'm_dsc' => 'a.time_updated DESC, a.time_created',
            'mc_dsc' => 'CASE WHEN (a.time_updated = ' . $db->quote($db->getNullDate()) . ') THEN a.time_created ELSE a.time_updated END',
            'c_dsc' => 'a.time_created',
            'p_dsc' => 'a.state',
            'random' => $db->getQuery(true)->Rand(),
        );

        $orderCol = ArrayHelper::getValue($order_map, $params->get('ordering'), 'a.state');
        $orderDirn = 'DESC';
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        } else {
            $query->order('a.id DESC');
        }
        $start = 0;
        $end = $params->get('count', 5);
        $db->setQuery($query, $start, $end);
        //echo($query->__toString());

        if ($results = $db->loadObjectList()) {
            return $results;
        } else {
            return array();
        }
    }

    public static function formatDate($date, $format = 'd/m/Y') {
        if ($date == '0000-00-00' || $date == '' || $date === '0000-00-00 00:00:00') {
            return false;
        }
        if ($format == '' || $format == null) {
            $format = 'Y-m-d H:i:s';
        }
        $date = date_create($date);
        return date_format($date, $format);
    }

}
