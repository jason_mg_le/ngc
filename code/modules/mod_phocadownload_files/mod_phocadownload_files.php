<?php

/*
 * @package Joomla 1.5
 * @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 * @module Phoca - Phoca Gallery Module
 * @copyright Copyright (C) Jan Pavelka www.phoca.cz
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @based on javascript: dTree 2.05 www.destroydrop.com/javascript/tree/
 * @copyright (c) 2002-2003 Geir Landr�
 */
defined('_JEXEC') or die('Restricted access'); // no direct access
if (!defined('DS'))
    define('DS', DIRECTORY_SEPARATOR);
if (!JComponentHelper::isEnabled('com_phocadownload', true)) {
    return JError::raiseError(JText::_('MOD_PHOCADOWNLOAD_TREE_PHOCA_DOWNLOAD_ERROR'), JText::_('MOD_PHOCADOWNLOAD_TREE_PHOCA_DOWNLOAD_IS_NOT_INSTALLED_ON_YOUR_SYSTEM'));
}

if (!class_exists('PhocaDownloadLoader')) {
    require_once( JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_phocadownload' . DS . 'libraries' . DS . 'loader.php');
}
phocadownloadimport('phocadownload.utils.settings');
phocadownloadimport('phocadownload.utils.utils');
phocadownloadimport('phocadownload.path.path');
phocadownloadimport('phocadownload.path.route');
phocadownloadimport('phocadownload.render.layout');
phocadownloadimport('phocadownload.file.file');
phocadownloadimport('phocadownload.file.fileupload');
phocadownloadimport('phocadownload.file.fileuploadmultiple');
phocadownloadimport('phocadownload.file.fileuploadsingle');
phocadownloadimport('phocadownload.download.download');
phocadownloadimport('phocadownload.render.renderfront');
phocadownloadimport('phocadownload.rate.rate');
phocadownloadimport('phocadownload.stat.stat');
phocadownloadimport('phocadownload.mail.mail');
phocadownloadimport('phocadownload.pagination.pagination');
phocadownloadimport('phocadownload.ordering.ordering');
phocadownloadimport('phocadownload.access.access');
phocadownloadimport('phocadownload.category.category');
phocadownloadimport('phocadownload.user.user');
phocadownloadimport('phocadownload.log.log');
phocadownloadimport('phocadownload.utils.utils');

$cateid = $params->get('cateid');

include_once JPATH_ROOT . '/components/com_phocadownload/models/category.php';
$model = JModelLegacy::getInstance('Category', 'PhocaDownloadModel', array('ignore_request' => true));

$files = $model->getFileList($cateid, 0);


require(JModuleHelper::getLayoutPath('mod_phocadownload_files'));
?>