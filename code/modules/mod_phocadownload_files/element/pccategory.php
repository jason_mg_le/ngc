<?php

/*
 * Created on : Nov 24, 2017, 3:35:00 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
defined('JPATH_BASE') or die;
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

jimport('joomla.form.formfield');

/**
 * Supports a modal category picker.
 *
 *
 */
class JFormFieldPcCategory extends JFormField {

    var $type = 'pccategory';

    /**
     * Method to get the field input markup.
     *
     * @return	string	The field input markup.
     * @since	1.6
     */
    protected function getInput() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        // Select the required fields from the table.
        $query->select('*');
        $table = '`#__phocadownload_categories`';
        
        $query->from($table);
        $db->setQuery($query);
        $items = $db->loadObjectList();

        $html = '<select class="inputbox"   name="' . $this->name . '" >';
        $html .= '<option value="0">' . JText::_('MOD_PHOCADOWNLOAD_FILES_CATEGORIES_LABEL') . '</option>';
        if (count($items) > 0) {
            $selected = '';
            foreach ($items as $key => $value) {
                $selected = $value->id == $this->value ? 'selected="selected"' : '';
                $html .= '<option value="' . $value->id . '" ' . $selected . '>' . $value->title . '</option>';
            }
        }
        //$html .= $categorylist;
        $html .= "</select>";
        return $html;
    }

}
