<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_tvmenuright
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', '')) {
    $id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>
<div class="form-group clearfix"></div>
<div class="box-right bgwhite quick_links">
    <div class="box-title brnone">
        <h1><?php echo $module->title; ?></h1>
    </div>
    <div class="box-content">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <ul class="nav flex-column menu<?php echo $class_sfx; ?>"<?php echo $id; ?>>
                    <?php
                    $c = count($list);
                    $r = 6;
                    $j = 1;
                    foreach ($list as $i => &$item) {
                        $class = 'nav-item item-' . $item->id;

                        if ($item->id == $default_id) {
                            $class .= ' default';
                        }

                        if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id)) {
                            $class .= ' current';
                        }

                        if (in_array($item->id, $path)) {
                            $class .= ' active';
                        } elseif ($item->type === 'alias') {
                            $aliasToId = $item->params->get('aliasoptions');

                            if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
                                $class .= ' active';
                            } elseif (in_array($aliasToId, $path)) {
                                $class .= ' alias-parent-active';
                            }
                        }

                        if ($item->type === 'separator') {
                            $class .= ' divider';
                        }

                        if ($item->deeper) {
                            $class .= ' deeper';
                        }

                        if ($item->parent) {
                            $class .= ' parent';
                        }

                        echo '<li class="' . $class . '">';

                        switch ($item->type) :
                            case 'separator':
                            case 'component':
                            case 'heading':
                            case 'url':
                                require JModuleHelper::getLayoutPath('mod_tvmenuright', 'default_' . $item->type);
                                break;

                            default:
                                require JModuleHelper::getLayoutPath('mod_tvmenuright', 'default_url');
                                break;
                        endswitch;

                        // The next item is deeper.
                        if ($item->deeper) {
                            echo '<div class="menu-dropdown">';
                            echo '<ul class="nav flex-column">';
                        }
                        // The next item is shallower.
                        elseif ($item->shallower) {
                            echo '</li>';
                            echo str_repeat('</ul></div></li>', $item->level_diff);
                        }
                        // The next item is on the same level.
                        else {
                            echo '</li>';
                        }

                        if ($j % $r == 0 && $j < $c) {
                            ?>
                        </ul>
                    </div>
                    <div class="item">
                        <ul class="nav flex-column">
                            <?php
                        }
                        $j++;
                    }
                    ?>

                </ul>

            </div>
        </div>
    </div>
</div>
