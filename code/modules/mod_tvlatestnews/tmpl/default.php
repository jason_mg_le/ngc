<?php
/*
 * Created on : Jan 24, 2018, 9:30:18 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
?>
<div class="box-slide bgwhite <?php echo $moduleclass_sfx; ?>">
    <div class="box-title">
        <h1><img src="images/icon-latest.png" alt=""/> <span><?php echo $module->title; ?></span></h1>
    </div>
    <div class="box-content pl20">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <ul class="nav flex-column">
                    <?php
                    $c = count($list);
                    $r = ceil($c / 3);
                    $r = 5;
                    $i = 1;
                    if (count($c) > 0) {
                        foreach ($list as $item) {
                            $image = json_decode($item->images);
                            ?>
                            <li>
                                <div class="row">
                                    <div class="col-3">
                                        <?php if ($image->image_intro != '') : ?>
                                            <a href="<?php echo $item->link; ?>">
                                                <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . $image->image_intro . '&w=58&h=57&q=85'; ?>" width="309"/>
                                            </a> 
                                        <?php else: ?>
                                            <a href="<?php echo $item->link; ?>">
                                                <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . 'components/com_events/assets/images/noimageevent.png&w=58&h=57&q=85'; ?>" width="309"/>
                                            </a> 
                                        <?php endif; ?>
                                    </div>    
                                    <div class="col-9">
                                        <h2><a href="<?php echo $item->link; ?>"><?php echo $item->title ?></a></h2>
                                        <p class="text-fade">
                                            Posted on <?php echo ModArticlesTvLatestHelper::formatDate($item->created, 'd/m/Y'); ?>
                                        </p>
                                    </div>    
                                </div>
                            </li>
                            <?php
                            if ($i % $r == 0 && $i < $c) {
                                ?>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="nav flex-column">
                                <?php
                            }
                            $i++;
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        <a class="go-laltest" href="<?php echo JRoute::_($params->get('link_latestnews', '#')); ?>">&nbsp;</a>
        <!--<a class="go-laltest" href="<?php //echo JRoute::_(ContentHelperRoute::getCategoryRoute($params->get('catid', 8))); ?>">&nbsp;</a>-->
    </div>
</div>