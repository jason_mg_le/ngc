CREATE TABLE IF NOT EXISTS `#__events_event` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255) COLLATE utf8_bin NOT NULL ,
`date` DATE NOT NULL ,
`time` VARCHAR(20)  NOT NULL ,
`date_end` DATE NOT NULL ,
`address` VARCHAR(255)  NOT NULL ,
`description` LONGTEXT NOT NULL ,
`images` TEXT NOT NULL ,
`repeat_method` VARCHAR(255)  NOT NULL ,
`repeat` VARCHAR(255)  NOT NULL ,
`week` VARCHAR(255)  NOT NULL ,
`month` VARCHAR(255)  NOT NULL ,
`month_type` VARCHAR(255)  NOT NULL ,
`monthly_list` VARCHAR(255)  NOT NULL ,
`month_week` VARCHAR(255)  NOT NULL ,
`year_month` VARCHAR(255)  NOT NULL ,
`time_created` DATETIME NOT NULL ,
`time_updated` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__events_assign` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`event_id` INT(11)  NOT NULL ,
`user_id` INT(11)  NOT NULL ,
`time_created` DATETIME NOT NULL ,
`time_updated` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;




