<?php
namespace Twilio\Rest\Api\V2010\Account;

/**
 *
 * @package JCHAT::FRAMEWORK::administrator::components::com_jchat
 * @subpackage framework
 * @subpackage twilio
 * @subpackage sdk
 * @author Joomla! Extensions Store
 * @copyright (C) 2015 - Joomla! Extensions Store
 * @license GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );

use Twilio\ListResource;
use Twilio\Options;
use Twilio\Values;
use Twilio\Version;

class TokenList extends ListResource {
    /**
     * Construct the TokenList
     * 
     * @param Version $version Version that contains the resource
     * @param string $accountSid The unique sid that identifies this account
     * @return \Twilio\Rest\Api\V2010\Account\TokenList 
     */
    public function __construct(Version $version, $accountSid) {
        parent::__construct($version);
        
        // Path Solution
        $this->solution = array(
            'accountSid' => $accountSid,
        );
        
        $this->uri = '/Accounts/' . rawurlencode($accountSid) . '/Tokens.json';
    }

    /**
     * Create a new TokenInstance
     * 
     * @param array|Options $options Optional Arguments
     * @return TokenInstance Newly created TokenInstance
     */
    public function create($options = array()) {
        $options = new Values($options);
        
        $data = Values::of(array(
            'Ttl' => $options['ttl'],
        ));
        
        $payload = $this->version->create(
            'POST',
            $this->uri,
            array(),
            $data
        );
        
        return new TokenInstance(
            $this->version,
            $payload,
            $this->solution['accountSid']
        );
    }

    /**
     * Provide a friendly representation
     * 
     * @return string Machine friendly representation
     */
    public function __toString() {
        return '[Twilio.Api.V2010.TokenList]';
    }
}