<?php
namespace Twilio;

/**
 *
 * @package JCHAT::FRAMEWORK::administrator::components::com_jchat
 * @subpackage framework
 * @subpackage twilio
 * @subpackage sdk
 * @author Joomla! Extensions Store
 * @copyright (C) 2015 - Joomla! Extensions Store
 * @license GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );

class VersionInfo {
    const MAJOR = 5;
    const MINOR = 4;
    const PATCH = 2;

    public static function string() {
        return implode('.', array(self::MAJOR, self::MINOR, self::PATCH));
    }
}
