<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Employees helper.
 *
 * @since  1.6
 */
class EmployeesHelper {

    /**
     * Configure the Linkbar.
     *
     * @param   string  $vName  string
     *
     * @return void
     */
    public static function addSubmenu($vName = '') {
        JHtmlSidebar::addEntry(
                JText::_('COM_EMPLOYEES_TITLE_EMPLOYEES'), 'index.php?option=com_employees&view=employees', $vName == 'employees'
        );
    }

    /**
     * Gets the files attached to an item
     *
     * @param   int     $pk     The item's id
     *
     * @param   string  $table  The table's name
     *
     * @param   string  $field  The field's name
     *
     * @return  array  The files
     */
    public static function getFiles($pk, $table, $field) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
                ->select($field)
                ->from($table)
                ->where('id = ' . (int) $pk);

        $db->setQuery($query);

        return explode(',', $db->loadResult());
    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return    JObject
     *
     * @since    1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_employees';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }

    public static function add($post = array()) {
        include_once JPATH_COMPONENT . '/com_employees/models/employee.php';
        $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        include_once JPATH_COMPONENT . '/com_employees/models/employee.php';
        $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete($id) {
        if ($id > 0) {
            include_once JPATH_COMPONENT . '/com_employees/models/employee.php';
            $userAssign = JModelLegacy::getInstance('Employee', 'EmployeesModel', array('ignore_request' => true));
            $objTable = $userAssign->getTable();
            $objTable->delete($id);
        }
        return true;
    }

    function formatDate($date, $format = 'd/m/Y') {
        if ($date == '0000-00-00' || $date == '' || $date === '0000-00-00 00:00:00') {
            return false;
        }
        if ($format == '' || $format == null) {
            $format = 'Y-m-d H:i:s';
        }
        $date = date_create($date);
        return date_format($date, $format);
    }

}
