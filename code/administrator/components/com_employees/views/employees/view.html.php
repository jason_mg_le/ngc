<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Employees.
 *
 * @since  1.6
 */
class EmployeesViewEmployees extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		EmployeesHelper::addSubmenu('employees');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = EmployeesHelper::getActions();

		JToolBarHelper::title(JText::_('COM_EMPLOYEES_TITLE_EMPLOYEES'), 'employees.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/employee';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('employee.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('employees.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('employee.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('employees.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('employees.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'employees.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('employees.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('employees.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'employees.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('employees.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_employees');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_employees&view=employees');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`first_name`' => JText::_('COM_EMPLOYEES_EMPLOYEES_FIRST_NAME'),
			'a.`last_name`' => JText::_('COM_EMPLOYEES_EMPLOYEES_LAST_NAME'),
			'a.`indentification`' => JText::_('COM_EMPLOYEES_EMPLOYEES_INDENTIFICATION'),
			'a.`nationality`' => JText::_('COM_EMPLOYEES_EMPLOYEES_NATIONALITY'),
			'a.`race`' => JText::_('COM_EMPLOYEES_EMPLOYEES_RACE'),
			'a.`mobile`' => JText::_('COM_EMPLOYEES_EMPLOYEES_MOBILE'),
			'a.`email`' => JText::_('COM_EMPLOYEES_EMPLOYEES_EMAIL'),
			'a.`address`' => JText::_('COM_EMPLOYEES_EMPLOYEES_ADDRESS'),
			'a.`address2`' => JText::_('COM_EMPLOYEES_EMPLOYEES_ADDRESS2'),
			'a.`title`' => JText::_('COM_EMPLOYEES_EMPLOYEES_TITLE'),
			'a.`department`' => JText::_('COM_EMPLOYEES_EMPLOYEES_DEPARTMENT'),
			'a.`deparment_manager`' => JText::_('COM_EMPLOYEES_EMPLOYEES_DEPARMENT_MANAGER'),
			'a.`office_email`' => JText::_('COM_EMPLOYEES_EMPLOYEES_OFFICE_EMAIL'),
			'a.`office`' => JText::_('COM_EMPLOYEES_EMPLOYEES_OFFICE'),
		);
	}

    /**
     * Check if state is set
     *
     * @param   mixed  $state  State
     *
     * @return bool
     */
    public function getState($state)
    {
        return isset($this->state->{$state}) ? $this->state->{$state} : false;
    }
}
