
/*
 * Created on : Jan 19, 2018, 11:38:47 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    var owlCarousel = $('.owl-carousel');
    if (owlCarousel.length) {
        owlCarousel.owlCarousel({
            items: 1,
            margin: 10,
            nav: false,
            loop: true
        })
    }

    $('.inbox-announcement .box-mess  h3').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.box-mess');
        if ($parent.is('.read-more')) {
            $parent.removeClass('unread');
            $parent.removeClass('read-more');
            $parent.find('.content-mess').hide();
        } else {
            $parent.removeClass('unread');
            $parent.addClass('read-more');
            $parent.find('.content-mess').show();
        }

    })

    function updateContent($eleupdate, $url) {
        $(".body").LoadingOverlay("show");
        $.ajax({
            url: $url,
            success: function (data, textStatus, jqXHR) {
                $(".body").LoadingOverlay("hide");
                $($eleupdate).html(data)
            }
        })
    }
}
)