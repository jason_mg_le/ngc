<?php
defined('_JEXEC') or die;

/** @var JDocumentHtml $this */
$app = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');

if ($task === 'edit' || $layout === 'form') {
    $fullWidth = 1;
} else {
    $fullWidth = 0;
}

// Logo file or site title param
if ($this->params->get('logoFile')) {
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($this->params->get('sitetitle')) {
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
} else {
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <script src="<?php echo JUri::base(); ?>/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <jdoc:include type="head" />
    <?php include_once 'header.php'; ?>
</head>
<body class="site <?php
echo $option
 . ' view-' . $view
 . ($layout ? ' layout-' . $layout : ' no-layout')
 . ($task ? ' task-' . $task : ' no-task')
 . ($itemid ? ' itemid-' . $itemid : '')
 . ($params->get('fluidContainer') ? ' fluid' : '');
echo ($this->direction === 'rtl' ? ' rtl' : '');
?>">
    <!-- Body -->
    <div class="body" id="top">
        <?php if ($this->countModules('menuleft')) { ?>
            <jdoc:include type="modules" name="menuleft" style="none"/>                        
        <?php } ?>

        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <a href="<?php echo JUri::base(); ?>">
                            <?php echo $logo ?>
                        </a>
                    </div>
                    <div class="col-md-5">
                        <?php if ($this->countModules('search-box')) { ?>
                            <jdoc:include type="modules" name="search-box" style="none"/>                        
                        <?php } ?>

                    </div>
                    <div class="col-md-4">
                        <?php if ($this->countModules('menutop')) { ?>
                            <jdoc:include type="modules" name="menutop" style="none"/>                        
                        <?php } ?>                        
                    </div>
                </div>
            </div>
        </header>
        <section class="tvmain">
            <div class="form-group clearfix">&nbsp;</div>
            <div class="container">
                <div class="row">
                    <?php
                    $mainClass = '';
                    if ($this->countModules('sidebar-b')) {
                        $mainClass = 'col-lg-8';
                    } else {
                        $mainClass = 'col-lg-12';
                    }
                    ?>
                    <div class="<?php echo $mainClass ?>">
                        <?php if ($this->countModules('slideshow')) { ?>
                            <jdoc:include type="modules" name="slideshow" style="none"/>                        
                        <?php } ?> 
                        <?php if ($this->countModules('banner')) { ?>
                            <jdoc:include type="modules" name="banner" style="none"/>                        
                        <?php } ?> 
                        <?php
                        $middleClass = '';
                        if ($this->countModules('middle-a') && $this->countModules('middle-b')) {
                            $middleClass = 'col-md-6';
                        } else if ($this->countModules('middle-a')) {
                            $middleClass = 'col-md-12';
                        } else if ($this->countModules('middle-b')) {
                            $middleClass = 'col-md-12';
                        }
                        if ($middleClass != '') {
                            ?>
                            <div class="form-group clearfix"></div>
                            <div class="moduletable">
                                <div class="row">
                                    <div class="<?php echo $middleClass ?>">
                                        <?php if ($this->countModules('middle-a')) { ?>
                                            <jdoc:include type="modules" name="middle-a" style="none"/>                        
                                        <?php } ?> 
                                    </div>
                                    <div class="<?php echo $middleClass ?>">
                                        <?php if ($this->countModules('middle-b')) { ?>
                                            <jdoc:include type="modules" name="middle-b" style="none"/>                        
                                        <?php } ?> 
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <div class="main">
                            <jdoc:include type="message" />
                            <jdoc:include type="component" />
                        </div>
                    </div>
                    <?php if ($mainClass != '') { ?>
                        <div class="col-lg-4 col-right">
                            <?php if ($this->countModules('sidebar-b')) { ?>
                                <jdoc:include type="modules" name="sidebar-b" style="none"/>                        
                            <?php } ?> 
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group clearfix">&nbsp;</div>
            <div class="form-group clearfix">&nbsp;</div>
        </section>
        <footer>
            <div class="container">
                <div class="row">
                    <?php if ($this->countModules('footer')) { ?>
                        <jdoc:include type="modules" name="footer" style="none"/>                        
                    <?php } ?> 
                </div>
                <?php if ($this->countModules('chatbox')) { ?>
                    <jdoc:include type="modules" name="chatbox" style="none"/>                        
                <?php } ?> 
            </div>
        </footer>
    </div>
<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
