<div class="bgwhite">
    <div class="tv-caption">
        <h5 class="disinline"><?php echo $this->escape($this->item->title); ?></h5>
        <span class="pull-right tv-date">
            <img src="<?php echo JUri::base(); ?>images/icon-calender.png" alt=""/><?php echo JText::_('COM_ARTICLE_POSTED_ON');?>
            <?php echo JHtml::_('date', $this->item->created, JText::_('d/m/Y')); ?>
        </span>
    </div>
    <div class="tv-content">
        <div class="bgwhite list-events">
            <div class="content-body">
                <?php
                $image = json_decode($this->item->images);
                ?>
                <?php if ($image->image_intro != '') : ?>
                    <img class="imgleft" src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . $image->image_intro . '&w=430&h=305&q=100'; ?>" alt=""/>
                <?php endif; ?>
                <?php echo $this->item->fulltext; ?>
            </div>
        </div>
    </div>
</div>