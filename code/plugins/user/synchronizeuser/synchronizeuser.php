<?php

/*
 * Created on : Sep 19, 2017, 5:12:37 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

jimport('joomla.utilities.date');

/**
 * An example custom profile plugin.
 *
 * @package		Joomla.Plugin
 * @subpackage	User.profile
 * @version		1.6
 */
class plgUserSynchronizeuser extends JPlugin {

    /**
     *
     * @param type $data
     * @param type $isNew
     * @param type $result
     * @param type $error
     * @return type 
     */
    function onUserAfterSave($data, $isNew, $result, $error) {

        include_once JPATH_ROOT . '/components/com_employees/helpers/employees.php';

        $user = JFactory::getUser();
        $userid = $user->id;
        $employeeId = EmployeesHelpersEmployees::getId($data['id']);
        $info_data = array(
            'id' => $employeeId,
            'state' => 1,
            'created_by' => $userid,
            'first_name' => $data['name'],
            'user_id' => $data['id']
        );
        $params = JComponentHelper::getParams('com_employees');
        $synusergroup = $params->get('synusergroup');
        $ischeck = false;
        if (count($synusergroup) > 0) {

            for ($i = 0; $i < count($synusergroup); $i++) {
                $arrGroups = $data['groups'];

                if (in_array($synusergroup[$i], $arrGroups)) {
                    $ischeck = true;
                }
            }
        }

        if ($ischeck) {
            if ($isNew) {
                $id_new = EmployeesHelpersEmployees::add($info_data);
            } else {
                $id_new = EmployeesHelpersEmployees::update($info_data);
            }
        }
        return true;
    }

    function onUserAfterDelete($user, $success, $msg) {

        include_once JPATH_ROOT . '/components/com_employees/helpers/employees.php';
        $employeeId = EmployeesHelpersEmployees::getId($user['id']);
        if ($employeeId > 0)
            $id_new = EmployeesHelpersEmployees::delete($employeeId);
        return true;
    }

}
