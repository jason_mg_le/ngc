<?php
// namespace components\com_jchat\views\lamessages;
/** 
 * @package JCHAT::LAMESSAGES::administrator::components::com_jchat
 * @subpackage views
 * @subpackage lamessages
 * @author Joomla! Extensions Store
 * @Copyright (C) 2015 - Joomla! Extensions Store
 * @license GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html 
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );  
define ( 'INDEX_WORKED', 'worked');
define ( 'INDEX_CLOSED', 'closed_ticket');

/**
 * User leaved messages view implementation
 *
 * @package JCHAT::MESSAGES::administrator::components::com_jchat
 * @subpackage views
 * @since 2.0
 */
class JChatViewLamessages extends JChatView {
	/**
	 * Default listEntities
	 * @access public
	 */
	public function display($tpl = 'list') {
		$doc = JFactory::getDocument ();
		$this->loadJQuery($doc);
		$this->loadJQueryUI($doc);
		$this->doc->addStyleSheet ( JUri::root(true) . '/components/com_jchat/css/bootstrap-interface.css' );
		$this->doc->addScript ( JUri::root ( true ) . '/components/com_jchat/js/bootstrap-interface.js' );
		
		// Inject js translations
		$translations = array('COM_JCHAT_SELECT_RECORD');
		$this->injectJsTranslations($translations, $doc);
		
		// Get main records
		$rows = $this->get('Data');
		$lists = $this->get('Filters');
		$total = $this->get('Total');
		
		$orders = array();
		$orders['order'] = $this->getModel()->getState('order');
		$orders['order_Dir'] = $this->getModel()->getState('order_dir');
		// Pagination view object model state populated
		$pagination = new JPagination( $total, $this->getModel()->getState('limitstart'), $this->getModel()->getState('limit') );
		$dates = array('start'=>$this->getModel()->getState('fromPeriod'), 'to'=>$this->getModel()->getState('toPeriod'));
		 
		$this->pagination = $pagination;
		$this->order = $this->getModel()->getState('order');
		$this->searchword = $this->getModel()->getState('searchword');
		$this->lists = $lists;
		$this->orders = $orders;
		$this->items = $rows;
		$this->option = $this->getModel()->getState('option');
		$this->dates = $dates;
		
		// Add meta info
		$this->_prepareDocument();
		
		parent::display($tpl);
	}

	/**
	 * Mostra la visualizzazione dettaglio del record singolo
	 * @param Object& $row
	 * @access public
	 */
	public function editEntity(&$row) {
		// Sanitize HTML Object2Form
		JFilterOutput::objectHTMLSafe( $row );
		
		// Load JS Client App dependencies
		$doc = JFactory::getDocument();
		$base = JUri::root();
		$this->loadJQuery($doc);
		$this->loadJQueryUI($doc);
		$this->loadValidation($doc);
		
		$this->doc->addStyleSheet ( JUri::root(true) . '/components/com_jchat/css/bootstrap-interface.css' );
		$this->doc->addScript ( JUri::root ( true ) . '/components/com_jchat/js/bootstrap-interface.js' );
		
		// Inject js translations
		$translations = array( 'COM_JCHAT_VALIDATION_ERROR', 
							   'COM_JCHAT_VALIDATION_ERROR_SUBJECT' );
		$this->injectJsTranslations($translations, $doc);
		
		$lists = $this->getModel()->getLists($row);
		$this->record = $row;
		$this->lists = $lists;
		$this->editor = JFactory::getEditor();
		$this->nameOfUser = $this->user->name;
		
		// Add meta info
		$this->_prepareDocument();
		
		parent::display ( 'edit' );
	}
	
	/**
	 * Effettua l'output view del file in attachment al browser
	 *
	 * @access public
	 * @param string $contents
	 * @param int $size
	 * @param array& $fieldsFunctionTransformation
	 * @return void
	 */
	public function sendCSVMessages($data, &$fieldsFunctionTransformation) {
		$delimiter = ';';
		$enclosure = '"';
		// Clean dirty buffer
		ob_end_clean();
		// Open buffer
		ob_start();
		// Open out stream
		$outstream = fopen("php://output", "w");
		// Funzione di scrittura nell'output stream
		function __outputCSV(&$vals, $key, $userData) {
			// Fields value transformations 
			if(isset($vals[INDEX_WORKED])) {
				$vals[INDEX_WORKED] = $vals[INDEX_WORKED] == 1 ? JText::_('COM_JCHAT_YESWORKED') : JText::_('COM_JCHAT_NOWORKED');
			}
			if(isset($vals[INDEX_CLOSED])) {
				$vals[INDEX_CLOSED] = $vals[INDEX_CLOSED] == 1 ? JText::_('JYES') : JText::_('JNO');
			}
			
			fputcsv($userData[0], $vals, $userData[1], $userData[2]); // add parameters if you want
		}
		// Echo delle intestazioni
		__outputCSV($fieldsFunctionTransformation, null, array($outstream, $delimiter, $enclosure));
		// Output di tutti i records
		array_walk($data, "__outputCSV", array($outstream, $delimiter, $enclosure));
		fclose($outstream);
		// Recupero output buffer content
		$contents = ob_get_clean();
		$size = strlen($contents);
	
	
		header ( 'Pragma: public' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Expires: ' . gmdate ( 'D, d M Y H:i:s' ) . ' GMT' );
		header ( 'Content-Disposition: attachment; filename="tickets.csv"' );
		header ( 'Content-Type: text/plain' );
		header ( "Content-Length: " . $size );
		echo $contents;
			
		exit ();
	}
	
	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;
	
		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if(is_null($menu)) {
			return;
		}
	
		$this->params = new JRegistry;
		$this->params->loadString($menu->params);
	
		$title = $this->params->get('page_title', JText::_('COM_JCHAT_TICKETS'));
		$this->doc->setTitle($title);
	
		if ($this->params->get('menu-meta_description')) {
			$this->doc->setDescription($this->params->get('menu-meta_description'));
		}
	
		if ($this->params->get('menu-meta_keywords')) {
			$this->doc->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
	
		if ($this->params->get('robots')) {
			$this->doc->setMetadata('robots', $this->params->get('robots'));
		}
	}
	
	/**
	 * Class constructor
	 *
	 * @param array $config
	 */
	public function __construct($config = array()) {
		// Parent view object
		parent::__construct ( $config );
	
		$joomlaConfig = JFactory::getConfig ();
		$this->joomlaConfig = $this->user->getParam ( 'timezone', $joomlaConfig->get ( 'offset' ) );
	}
}