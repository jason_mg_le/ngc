<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Inbox
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
$user = JFactory::getUser();
$userId = $user->get('id');
$HelpersInbox = new InboxHelpersInbox();

$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'components/com_inbox/assets/css/calendar.css');
$document->addScript(JUri::root() . 'components/com_inbox/assets/js/loadingoverlay.js');
$document->addScript(JUri::root() . 'components/com_inbox/assets/js/com_inbox.js');
?>
<div class="inbox-page">
    <div class="inbox-header bgwhite">
        <h5 class="disinline"><?php echo JText::_("COM_INBOX_TITLE_INBOX"); ?></h5><span class="icon-mess"></span><span class="text-mess"><?php echo JText::sprintf('COM_INBOX_MESSAGES_COUNT', $HelpersInbox->getTotalIsread()); ?></span>
        <div class="pull-right">
            <a href="<?php echo JRoute::_('index.php?option=com_inbox&view=messages') ?>" class="all-mess bold"><?php echo JText::_('COM_INBOX_MESSAGES_ALL'); ?></a>
            <a href="<?php echo JRoute::_('index.php?option=com_inbox&view=unread'); ?>" class="unread-mess"><?php echo JText::_('COM_INBOX_MESSAGES_UNREAD'); ?></a>
            <a href="<?php echo JRoute::_('index.php?option=com_inbox&view=newest'); ?>" class="newest-mess"><?php echo JText::_('COM_INBOX_MESSAGES_NEWEST'); ?><span class="tv-desc">&nbsp;</span></a>
        </div>
    </div>
    <div class="list-inbox">
        <?php foreach ($this->items as $i => $item) : ?>
            <div class="bgwhite pd1015 inbox-mess">
                <div class="box-mess <?php echo $item->isread == 0 ? 'unread' : '' ?>">
                    <h3 action="<?php echo $item->isread == 0 ? JRoute::_('index.php?option=com_inbox&view=message&task=message.isread&id=' . (int) $item->id) : '' ?>"><?php echo $this->escape($item->title); ?> <span class="pull-right mess-date"><?php echo $HelpersInbox->formatDate($item->time_created, 'd/m/Y H:i:s a'); ?></span><span class="more-less"></span></h3>
                    <div class="sub-mess"><?php echo $HelpersInbox->wordLimit($item->messages, 20); ?></div>
                    <div class="content-mess">
                        <?php echo $item->messages; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php echo $this->pagination->getListFooter(); ?>