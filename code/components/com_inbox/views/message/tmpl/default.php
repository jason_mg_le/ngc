<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Inbox
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_ALIAS'); ?></th>
			<td><?php echo $this->item->alias; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_MESSAGES'); ?></th>
			<td><?php echo $this->item->messages; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_TIME_CREATED'); ?></th>
			<td><?php echo $this->item->time_created; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_TIME_UPDATED'); ?></th>
			<td><?php echo $this->item->time_updated; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_INBOX_FORM_LBL_MESSAGE_ISREAD'); ?></th>
			<td><?php echo $this->item->isread; ?></td>
		</tr>

	</table>

</div>

