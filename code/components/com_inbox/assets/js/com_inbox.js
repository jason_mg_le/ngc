
/*
 * Created on : Jan 23, 2018, 9:19:30 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */


/*
 * Created on : Jan 23, 2018, 9:03:25 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    $(document).on('click', '.box-mess > h3', function (e) {
        e.preventDefault();
        var $me = $(this);
        var $parent = $(this).closest('.box-mess');
        if ($parent.is('.unread')) {
//            $parent.removeClass('unread');
//            $parent.addClass('read-more');
//            $parent.find('.content-mess').show();
            /*
             * Chay ajax cap nhat trang thai da doc
             */
            var $url = $me.attr('action');
            $(".body").LoadingOverlay("show");
            $.ajax({
                url: $url,
                success: function (data, textStatus, jqXHR) {
                    $(".body").LoadingOverlay("hide");
                    $parent.removeClass('unread');
                    $parent.addClass('read-more');
                    $parent.find('.sub-mess').hide();
                    $parent.find('.content-mess').show();
                }
            })
        } else {
            if ($parent.is('.read-more')) {
                $parent.removeClass('unread');
                $parent.removeClass('read-more');
                $parent.find('.sub-mess').show();
                $parent.find('.content-mess').hide();
            } else {
                $parent.removeClass('unread');
                $parent.addClass('read-more');
                $parent.find('.sub-mess').hide();
                $parent.find('.content-mess').show();
            }
        }
    })
    /*
    $(document).on('click', '.all-mess', function (e) {
        e.preventDefault();
        var $me = $(this);
        var $url = $me.attr('href');
        updateContent('.inbox-page', $url)
    })
    $(document).on('click', '.unread-mess', function (e) {
        e.preventDefault();
        var $me = $(this);
        var $url = $me.attr('href');
        updateContent('.inbox-page', $url)
    })
    $(document).on('click', '.newest-mess', function (e) {
        e.preventDefault();
        var $me = $(this);
        var $url = $me.attr('href');
        updateContent('.inbox-page', $url)
    })*/
    function updateContent($eleupdate, $url) {
        $(".body").LoadingOverlay("show");
        $.ajax({
            url: $url,
            success: function (data, textStatus, jqXHR) {
                $(".body").LoadingOverlay("hide");
                $($eleupdate).html(data)
            }
        })
    }
});