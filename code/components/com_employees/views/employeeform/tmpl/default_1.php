<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_employees', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_employees/js/form.js');

$user = JFactory::getUser();
$canEdit = EmployeesHelpersEmployees::canUserEdit($this->item, $user);
?>

<div class="employee-edit front-end-edit">
    <?php if (!$canEdit) : ?>
        <h3>
            <?php throw new Exception(JText::_('COM_EMPLOYEES_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
        </h3>
    <?php else : ?>
        <?php if (!empty($this->item->id)): ?>
            <h1><?php echo JText::sprintf('COM_EMPLOYEES_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
        <?php else: ?>
            <h1><?php echo JText::_('COM_EMPLOYEES_ADD_ITEM_TITLE'); ?></h1>
        <?php endif; ?>

        <form id="form-employee"
              action="<?php echo JRoute::_('index.php?option=com_employees&task=employee.save'); ?>"
              method="post" class="form-validate form-horizontal" enctype="multipart/form-data">

            <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

            <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

            <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

            <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

            <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

            <?php echo $this->form->getInput('created_by'); ?>
            <?php echo $this->form->getInput('modified_by'); ?>
            <?php echo $this->form->renderField('first_name'); ?>

            <?php echo $this->form->renderField('last_name'); ?>

            <?php echo $this->form->renderField('indentification'); ?>

            <?php echo $this->form->renderField('nationality'); ?>

            <?php echo $this->form->renderField('race'); ?>

            <?php echo $this->form->renderField('mobile'); ?>

            <?php echo $this->form->renderField('email'); ?>

            <?php echo $this->form->renderField('address'); ?>

            <?php echo $this->form->renderField('address2'); ?>

            <?php echo $this->form->renderField('title'); ?>

            <?php echo $this->form->renderField('department'); ?>

            <?php echo $this->form->renderField('deparment_manager'); ?>

            <?php echo $this->form->renderField('office_email'); ?>

            <?php echo $this->form->renderField('office'); ?>

            <?php echo $this->form->renderField('joined_since'); ?>

            <?php echo $this->form->renderField('time_created'); ?>

            <?php echo $this->form->renderField('time_updated'); ?>

            <?php echo $this->form->renderField('user_id'); ?>

            <?php echo $this->form->renderField('image'); ?>

            <?php if (!empty($this->item->image)) : ?>
                <?php $imageFiles = array(); ?>
                <?php foreach ((array) $this->item->image as $fileSingle) : ?>
                    <?php if (!is_array($fileSingle)) : ?>
                        <a href="<?php echo JRoute::_(JUri::root() . 'uploads' . DIRECTORY_SEPARATOR . $fileSingle, false); ?>"><?php echo $fileSingle; ?></a> | 
                        <?php $imageFiles[] = $fileSingle; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <input type="hidden" name="jform[image_hidden]" id="jform_image_hidden" value="<?php echo implode(',', $imageFiles); ?>" />
            <div class="control-group">
                <div class="controls">

                    <?php if ($this->canSave): ?>
                        <button type="submit" class="validate btn btn-primary">
                            <?php echo JText::_('JSUBMIT'); ?>
                        </button>
                    <?php endif; ?>
                    <a class="btn"
                       href="<?php echo JRoute::_('index.php?option=com_employees&task=employeeform.cancel'); ?>"
                       title="<?php echo JText::_('JCANCEL'); ?>">
                           <?php echo JText::_('JCANCEL'); ?>
                    </a>
                </div>
            </div>

            <input type="hidden" name="option" value="com_employees"/>
            <input type="hidden" name="task"
                   value="employeeform.save"/>
                   <?php echo JHtml::_('form.token'); ?>
        </form>
    <?php endif; ?>
</div>
