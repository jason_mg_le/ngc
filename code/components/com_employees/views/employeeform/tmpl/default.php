<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_employees', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_employees/js/form.js');
$doc->addScript(JUri::base() . 'components/com_employees/assets/js/jquery.mask.min.js');
$doc->addScript(JUri::base() . 'components/com_employees/assets/js/com_employees.js');
$doc->addStyleSheet(JUri::root() . 'components/com_employees/assets/css/jquery.fileupload.css');

$user = JFactory::getUser();
$canEdit = EmployeesHelpersEmployees::canUserEdit($this->item, $user);
?>
<div class="bgwhite intranet_profile">
    <form id="form-employee"
          action="<?php echo JRoute::_('index.php?option=com_employees&task=employee.save'); ?>"
          method="post" class="form-validate form-horizontal" enctype="multipart/form-data">

        <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

        <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

        <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

        <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

        <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
        <div class="caption">
            <h5 class=""><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_MY_PERSONAL_DETAILS'); ?></h5>
        </div>
        <div class="row pd2030">
            <div class="col-md-9">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_FIRST_NAME'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->first_name; ?>"  name="jform[first_name]]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_LAST_NAME'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->last_name; ?>"  name="jform[last_name]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_INDENTIFICATION'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->indentification; ?>"  name="jform[indentification]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_NATIONALITY'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->nationality; ?>"  name="jform[nationality]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_RACE'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->race; ?>"  name="jform[race]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_MOBILE'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->mobile; ?>"  name="jform[mobile]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_EMAIL'); ?></label>
                    <div class="col-sm-10">
                        <input type="email" value="<?php echo $this->item->email; ?>"  name="jform[email]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_ADDRESS'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->address; ?>"  name="jform[address]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">&nbsp;</label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->address2; ?>"  name="jform[address2]" class="form-control" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <?php if (!empty($this->item->image)) : ?>
                    <?php $imageFiles = array(); ?>
                    <?php foreach ((array) $this->item->image as $fileSingle) : ?>
                        <?php if (!is_array($fileSingle)) : ?>
                            <img src="<?php echo JRoute::_(JUri::root() . 'uploads' . DIRECTORY_SEPARATOR . $fileSingle, false); ?>" style="max-width: 133px;"/>
                            <?php $imageFiles[] = $fileSingle; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <input type="hidden" name="jform[image_hidden]" id="jform_image_hidden" value="<?php echo implode(',', $imageFiles); ?>" />
                <?php endif; ?>
                <br/>
                <br/>
                <span class="btn tv-btn btn-primary fileinput-button">
                    <!--<i class="fa fa-cloud-upload"></i>-->
                    <span><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_UPLOAD_IMAGE'); ?></span>                               
                    <input id="fileupload" type="file" name="jform[image][]" multiple>
                </span>
            </div>
        </div>
        <div class="tv-title">
            <h5 class=""><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_JOB_DETAILS'); ?></h5>
        </div>
        <div class="row pd2030">
            <div class="col-md-9">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_TITLE'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->title; ?>"  name="jform[title]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_DEPARTMENT'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->department; ?>"  name="jform[department]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_DEPARMENT_MANAGER'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->deparment_manager; ?>"  name="jform[deparment_manager]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_OFFICE_EMAIL'); ?></label>
                    <div class="col-sm-10">
                        <input type="email" value="<?php echo $this->item->office_email; ?>"  name="jform[office_email]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_OFFICE'); ?></label>
                    <div class="col-sm-10">
                        <input type="type" value="<?php echo $this->item->office; ?>"  name="jform[office]" class="form-control" placeholder=""/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_JOINED_SINCE'); ?></label>
                    <div class="col-sm-10">
                        <input id="joined_since" type="type" value="<?php echo EmployeesHelpersEmployees::formatDate($this->item->joined_since, 'd/m/Y'); ?>"  name="jform[joined_since]" class="form-control" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="action col-md-12">
                <input type="hidden" name="option" value="com_employees"/>
                <input type="hidden" name="task"
                       value="employeeform.save"/>
                       <?php echo JHtml::_('form.token'); ?>
                <button class="btn btn-danger"><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_SAVE_CHANGE'); ?></button>
            </div>
        </div>
    </form>
</div>

