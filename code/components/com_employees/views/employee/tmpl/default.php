<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Employees
 * @author     Thang Tran <thang.testdev@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_employees');

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_employees'))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_FIRST_NAME'); ?></th>
			<td><?php echo $this->item->first_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_LAST_NAME'); ?></th>
			<td><?php echo $this->item->last_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_INDENTIFICATION'); ?></th>
			<td><?php echo $this->item->indentification; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_NATIONALITY'); ?></th>
			<td><?php echo $this->item->nationality; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_RACE'); ?></th>
			<td><?php echo $this->item->race; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_MOBILE'); ?></th>
			<td><?php echo $this->item->mobile; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_EMAIL'); ?></th>
			<td><?php echo $this->item->email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_ADDRESS'); ?></th>
			<td><?php echo $this->item->address; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_ADDRESS2'); ?></th>
			<td><?php echo $this->item->address2; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_DEPARTMENT'); ?></th>
			<td><?php echo $this->item->department; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_DEPARMENT_MANAGER'); ?></th>
			<td><?php echo $this->item->deparment_manager; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_OFFICE_EMAIL'); ?></th>
			<td><?php echo $this->item->office_email; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_OFFICE'); ?></th>
			<td><?php echo $this->item->office; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_JOINED_SINCE'); ?></th>
			<td><?php echo $this->item->joined_since; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_TIME_CREATED'); ?></th>
			<td><?php echo $this->item->time_created; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_TIME_UPDATED'); ?></th>
			<td><?php echo $this->item->time_updated; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_USER_ID'); ?></th>
			<td><?php echo $this->item->user_id; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_EMPLOYEES_FORM_LBL_EMPLOYEE_IMAGE'); ?></th>
			<td>
			<?php
			foreach ((array) $this->item->image as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'uploads' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank">' . $singleFile . '</a> ';
				endif;
			endforeach;
		?></td>
		</tr>

	</table>

</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_employees&task=employee.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_EMPLOYEES_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_employees.employee.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_EMPLOYEES_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_EMPLOYEES_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_EMPLOYEES_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_employees&task=employee.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_EMPLOYEES_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>