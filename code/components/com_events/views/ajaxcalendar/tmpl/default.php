<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

include_once (JPATH_COMPONENT . '/helpers/calendar.php');

function Month_num($month_name) {
    for ($month_num = 1; $month_num <= 12; $month_num++) {
        if (date("F", mktime(0, 0, 0, $month_num, 1, 0)) == $month_name and ( $month_num < 10)) {
            return '0' . $month_num;
        } else {
            return $month_num;
        }
    }
}

$calendar = new Calendar();
$calendar->naviHref = JRoute::_('index.php?option=com_events&view=ajaxcalendar&format=raw');
$calendar->prev = '&nbsp;';
$calendar->next = '&nbsp;';
$date = JRequest::getVar("date", date("Y") . '-' . Month_num(date("F")) . '-' . date("d"));
$year = substr($date, 0, 4);
$month = substr($date, 5, 2);
$arrDays = $this->items['dates'];
$arrEvent = $this->items['tilte'];
//echo $month. $year;
echo $calendar->show($month, $year, $arrDays, $arrEvent);
?>



