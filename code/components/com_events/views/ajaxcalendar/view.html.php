<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Events.
 *
 * @since  1.6
 */
class EventsViewAjaxcalendar extends JViewLegacy {

    protected $items;

    /**
     * Display the view
     *
     * @param   string  $tpl  Template name
     *
     * @return void
     *
     * @throws Exception
     */
    public function display($tpl = null) {

        $this->items = $this->get('Items');


        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        parent::display($tpl);
    }

}
