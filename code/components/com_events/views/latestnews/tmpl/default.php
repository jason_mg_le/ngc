<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');
//str_replace('Itemid', 'Item', 
$HelpersEvents = new EventsHelpersEvents();
?>

<div class="latest-news">
    <div class="tv-title">
        <h5><?php echo JText::_('COM_EVENTS_LATEST_NEWS')?></h5>
    </div>
    <?php
    foreach ($this->items as $i => $item) :
        $image = json_decode($item->images);
        $item->slug = $item->id . ':' . $item->alias;
        $item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid, $item->language));
        if (strpos($item->link, 'Itemid')) {
            $link = explode('Itemid', $item->link)[0] . '?Itemid=lt';
            $item->link = $link;
        } else {
            $item->link = $item->link . '?Itemid=lt';
        }
        ?>
        <div class="media bgwhite list-events">
            <?php if ($image->image_intro != '') : ?>
                <a href="<?php echo $item->link; ?>">
                    <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . $image->image_intro . '&w=309&h=221&q=85'; ?>"/>
                </a> 
            <?php else: ?>
                <a href="<?php echo $item->link; ?>">
                    <img src="<?php echo 'modules/mod_tvlatestnews/timthumb.php?src=' . JUri::root() . 'components/com_events/assets/images/noimageevent.png&w=309&h=221&q=85'; ?>"/>
                </a> 
            <?php endif; ?>
            <div class="media-body">
                <h5 class=""><a href="<?php echo str_replace('Itemid', 'Item', JRoute::_(ContentHelperRoute::getArticleRoute($item->id, $item->catid, $item->language))); ?>"><?php echo $this->escape($item->title); ?></a></h5>
                <div><?php echo $HelpersEvents->wordLimit($item->introtext, 50); ?></div>
                <div class="text-fade">
                    <span>
                        <img src="images/icon-calender.png" alt=""/><?php echo JText::_('COM_EVENTS_POSTED_ON');?> 
                        <?php echo JHtml::_('date', $item->created, JText::_('d/m/Y')); ?>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <?php echo $this->pagination->getListFooter(); ?>
</div>



