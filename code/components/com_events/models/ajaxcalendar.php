<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use Joomla\CMS\Factory;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Events records.
 *
 * @since  1.6
 */
class EventsModelAjaxcalendar extends JModelList {

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @see        JController
     * @since      1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'modified_by', 'a.modified_by',
                'title', 'a.title',
                'alias', 'a.alias',
                'date', 'a.date',
                'time', 'a.time',
                'date_end', 'a.date_end',
                'address', 'a.address',
                'description', 'a.description',
                'images', 'a.images',
                'repeat_method', 'a.repeat_method',
                'repeat', 'a.repeat',
                'week', 'a.week',
                'month', 'a.month',
                'month_type', 'a.month_type',
                'monthly_list', 'a.monthly_list',
                'month_week', 'a.month_week',
                'year_month', 'a.year_month',
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param   string  $ordering   Elements order
     * @param   string  $direction  Order direction
     *
     * @return void
     *
     * @throws Exception
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null) {
        $app = Factory::getApplication();
        $list = $app->getUserState($this->context . '.list');

        $ordering = isset($list['filter_order']) ? $list['filter_order'] : null;
        $direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;

        $list['limit'] = (int) Factory::getConfig()->get('list_limit', 20);
        $list['start'] = $app->input->getInt('start', 0);
        $list['ordering'] = $ordering;
        $list['direction'] = $direction;

        $app->setUserState($this->context . '.list', $list);
        $app->input->set('list', null);

        // List state information.
        parent::populateState($ordering, $direction);

        $app = Factory::getApplication();

        $ordering = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

        if ($limit == 0) {
            $limit = $app->get('list_limit', 0);
        }

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return   JDatabaseQuery
     *
     * @since    1.6
     * UPDATE `tv_events_event` SET `date`=DATE_SUB((DATE_SUB(`date`, INTERVAL (-`id`) MONTH)), INTERVAL  -`id` DAY)
     */
    protected function getListQuery() {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $data = $_REQUEST;

        $query->select('a.*');
        $query->from('`#__events_event` AS a');

        $query->join('LEFT', '#__events_assign AS ea ON a.id=ea.event_id');

        $user = JFactory::getUser();
        $userId = $user->get('id');

        $query->where('ea.user_id = ' . $userId);


        $date = JRequest::getVar("date", date("Y") . '-' . $this->Month_num(date("F")) . '-' . date("d"));
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);

        $query->where("( ( (a.date<='" . $db->escape(substr($date, 0, 7)) . "-01' or a.date like '" . $db->escape(substr($date, 0, 7)) . "%') and  (a.date_end>='" . $db->escape(substr($date, 0, 7)) . "-01' ) or a.date_end='0000-00-00') or ( a.date_end is Null and a.date like '" . $db->escape(substr($date, 0, 7)) . "%' ) )  ");

        if (!Factory::getUser()->authorise('core.edit', 'com_events')) {
            $query->where('a.state = 1');
        }
        //echo $query;
        return $query;
    }

    function Month_num($month_name) {
        for ($month_num = 1; $month_num <= 12; $month_num++) {
            if (date("F", mktime(0, 0, 0, $month_num, 1, 0)) == $month_name and ( $month_num < 10)) {
                return '0' . $month_num;
            } else {
                return $month_num;
            }
        }
    }

    /**
     * Method to get an array of data items
     *
     * @return  mixed An array of data on success, false on failure.
     */
    public function getItems() {
        $items = parent::getItems();

        $id_array = array();

        $s = count($items);

        $array_days = array();
        $array_days1 = array();
        $title = array();
        $title1 = array();
        $eventIDs = array();

        $date = JRequest::getVar("date", date("Y") . '-' . $this->Month_num(date("F")) . '-' . date("d"));
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);

        $show_time = 1;

        for ($i = 1; $i <= $s; $i++) {

            if ($items[$i - 1]->date_end == '0000-00-00')
                $d_end = ((int) substr($items[$i - 1]->date, 0, 4) + 40) . substr($items[$i - 1]->date, 4, 6);
            else
                $d_end = $items[$i - 1]->date_end;

            $date_month = (int) substr($items[$i - 1]->date, 5, 2);
            $date_end_month = (int) substr($d_end, 5, 2);

            $date_day = (int) substr($items[$i - 1]->date, 8, 2);
            $date_end_day = (int) substr($d_end, 8, 2);
            //echo $date_day;
            $date_year_month = (int) (substr($items[$i - 1]->date, 0, 4) . substr($items[$i - 1]->date, 5, 2));
            $date_end_year_month = (int) (substr($d_end, 0, 4) . substr($d_end, 5, 2));

            $year_month = (int) ($year . $month);

            $date_days = array();
            $weekdays_start = array();
            $weekdays = array();

            $date_days[] = $date_day;
            $used = array();

            foreach ($date_days as $date_day) {
                if ($date_month == $month AND $year == substr($date_year_month, 0, 4)) {

                    if (in_array($date_day, $used))
                        continue;
                    else
                        array_push($used, $date_day);

                    if (in_array($date_day, $array_days)) {
                        $key = array_search($date_day, $array_days);
                        $title_num[$date_day] ++;

                        if ($items[$i - 1]->description != "")
                            $array_days1[$key] = $date_day;


                        $c = $title_num[$date_day];
                        if ($c <= 3) {
                            $list = '<p>';
                            if ($items[$i - 1]->date_end and $items[$i - 1]->date_end != $items[$i - 1]->date and $date_format != "" and $show_time != 0)
                                $list .= str_replace("d", substr($items[$i - 1]->date, 8, 2), str_replace("m", substr($items[$i - 1]->date, 5, 2), str_replace("y", substr($items[$i - 1]->date, 0, 4), $date_format))) . '&nbsp;-&nbsp;' . str_replace("d", substr($items[$i - 1]->date_end, 8, 2), str_replace("m", substr($items[$i - 1]->date_end, 5, 2), str_replace("y", substr($items[$i - 1]->date_end, 0, 4), $date_format)));

                            $list .= '<span class="tooltip-titile">' . $items[$i - 1]->title . '</span>';
                            if ($items[$i - 1]->time and $show_time != 0)
                                $list .= '<span class="tooltip-time">' . $items[$i - 1]->time . '</span></p>';

                            $title[$date_day] = $title[$date_day] . $list;
                            $eventIDs[$date_day] = $eventIDs[$date_day] . $items[$i - 1]->id . '<br>';
                        }

                        $title1[$date_day] = $title1[$date_day] . $items[$i - 1]->title . '<br>';
                    }
                    else {
                        $array_days[] = $date_day;
                        $key = array_search($date_day, $array_days);
                        if ($items[$i - 1]->description != "")
                            $array_days1[$key] = $date_day;

                        $title_num[$date_day] = 1;

                        $c = 1;

                        $list = '<p>';

                        if ($items[$i - 1]->date_end and $items[$i - 1]->date_end != $items[$i - 1]->date and $date_format != "" and $show_time != 0)
                            $list .= str_replace("d", substr($items[$i - 1]->date, 8, 2), str_replace("m", substr($items[$i - 1]->date, 5, 2), str_replace("y", substr($items[$i - 1]->date, 0, 4), $date_format))) . '&nbsp;-&nbsp;' . str_replace("d", substr($items[$i - 1]->date_end, 8, 2), str_replace("m", substr($items[$i - 1]->date_end, 5, 2), str_replace("y", substr($items[$i - 1]->date_end, 0, 4), $date_format)));

                        $list .= '<span class="tooltip-titile">' . $items[$i - 1]->title . '</span>';
                        if ($items[$i - 1]->time and $show_time != 0)
                            $list .= '<span class="tooltip-time">' . $items[$i - 1]->time . '</span></p>';

                        $title[$date_day] = $list;
                        $title1[$date_day] = $items[$i - 1]->title . '<br>';
                        $eventIDs[$date_day] = $items[$i - 1]->id . '<br>';
                    }

                    //$date_day=$date_day+$repeat;
                }
            }
        }
        $result = array('dates' => $array_days, 'tilte' => $title);

        return $result;
    }

    /**
     * Overrides the default function to check Date fields format, identified by
     * "_dateformat" suffix, and erases the field if it's not correct.
     *
     * @return void
     */
    protected function loadFormData() {
        $app = Factory::getApplication();
        $filters = $app->getUserState($this->context . '.filter', array());
        $error_dateformat = false;

        foreach ($filters as $key => $value) {
            if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null) {
                $filters[$key] = '';
                $error_dateformat = true;
            }
        }

        if ($error_dateformat) {
            $app->enqueueMessage(JText::_("COM_EVENTS_SEARCH_FILTER_DATE_FORMAT"), "warning");
            $app->setUserState($this->context . '.filter', $filters);
        }

        return parent::loadFormData();
    }

    /**
     * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
     *
     * @param   string  $date  Date to be checked
     *
     * @return bool
     */
    private function isValidDate($date) {
        $date = str_replace('/', '-', $date);
        return (date_create($date)) ? Factory::getDate($date)->format("Y-m-d") : null;
    }

}
