<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use Joomla\CMS\Factory;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Events records.
 *
 * @since  1.6
 */
class EventsModelLatestnews extends JModelList {

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param   string  $ordering   Elements order
     * @param   string  $direction  Order direction
     *
     * @return void
     *
     * @throws Exception
     *
     * @since    1.6
     */
    protected function populateState($ordering = 'ordering', $direction = 'ASC') {
        $app = JFactory::getApplication();

        // List state information
        $value = $app->input->get('limit', $app->get('list_limit', 0), 'uint');
        $this->setState('list.limit', $value);

        $value = $app->input->get('limitstart', 0, 'uint');
        $this->setState('list.start', $value);

        $value = $app->input->get('filter_tag', 0, 'uint');
        $this->setState('filter.tag', $value);

        $orderCol = $app->input->get('filter_order', 'a.ordering');

        if (!in_array($orderCol, $this->filter_fields)) {
            $orderCol = 'a.ordering';
        }

        $this->setState('list.ordering', $orderCol);

        $listOrder = $app->input->get('filter_order_Dir', 'ASC');

        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
            $listOrder = 'ASC';
        }

        $this->setState('list.direction', $listOrder);

        $params = $app->getParams();
        $this->setState('params', $params);
        $user = JFactory::getUser();

        if ((!$user->authorise('core.edit.state', 'com_content')) && (!$user->authorise('core.edit', 'com_content'))) {
            // Filter on published for those who do not have edit or edit.state rights.
            $this->setState('filter.published', 1);
        }

        $this->setState('filter.language', JLanguageMultilang::isEnabled());

        // Process show_noauth parameter
        if (!$params->get('show_noauth')) {
            $this->setState('filter.access', true);
        } else {
            $this->setState('filter.access', false);
        }

        $this->setState('layout', $app->input->getString('layout'));
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return   JDatabaseQuery
     *
     * @since    1.6
     */
    protected function getListQuery() {
        // Get the current user for authorisation checks
        $user = JFactory::getUser();

        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'DISTINCT a.id, a.title, a.alias, a.introtext, a.fulltext, ' .
                        'a.checked_out, a.checked_out_time, ' .
                        'a.catid, a.created, a.created_by, a.created_by_alias, ' .
                        // Published/archived article in archive category is treats as archive article
                        // If category is not published then force 0
                        'CASE WHEN c.published = 2 AND a.state > 0 THEN 2 WHEN c.published != 1 THEN 0 ELSE a.state END as state,' .
                        // Use created if modified is 0
                        'CASE WHEN a.modified = ' . $db->quote($db->getNullDate()) . ' THEN a.created ELSE a.modified END as modified, ' .
                        'a.modified_by, uam.name as modified_by_name,' .
                        // Use created if publish_up is 0
                        'CASE WHEN a.publish_up = ' . $db->quote($db->getNullDate()) . ' THEN a.created ELSE a.publish_up END as publish_up,' .
                        'a.publish_down, a.images, a.urls, a.attribs, a.metadata, a.metakey, a.metadesc, a.access, ' .
                        'a.hits, a.xreference, a.featured, a.language, ' . ' ' . $query->length('a.fulltext') . ' AS readmore, a.ordering'
                )
        );

        $query->from('#__content AS a');

        $params = $this->getState('params');
        $orderby_sec = $params->get('orderby_sec');

        // Join over the frontpage articles if required.
        if ($this->getState('filter.frontpage')) {
            if ($orderby_sec === 'front') {
                $query->select('fp.ordering');
                $query->join('INNER', '#__content_frontpage AS fp ON fp.content_id = a.id');
            } else {
                $query->where('a.featured = 1');
            }
        } elseif ($orderby_sec === 'front' || $this->getState('list.ordering') === 'fp.ordering') {
            $query->select('fp.ordering');
            $query->join('LEFT', '#__content_frontpage AS fp ON fp.content_id = a.id');
        }

        // Join over the categories.
        $query->select('c.title AS category_title, c.path AS category_route, c.access AS category_access, c.alias AS category_alias')
                ->select('c.published, c.published AS parents_published, c.lft')
                ->join('LEFT', '#__categories AS c ON c.id = a.catid');

        // Join over the users for the author and modified_by names.
        $query->select("CASE WHEN a.created_by_alias > ' ' THEN a.created_by_alias ELSE ua.name END AS author")
                ->select('ua.email AS author_email')
                ->join('LEFT', '#__users AS ua ON ua.id = a.created_by')
                ->join('LEFT', '#__users AS uam ON uam.id = a.modified_by');

        // Join over the categories to get parent category titles
        $query->select('parent.title as parent_title, parent.id as parent_id, parent.path as parent_route, parent.alias as parent_alias')
                ->join('LEFT', '#__categories as parent ON parent.id = c.parent_id');

        if (JPluginHelper::isEnabled('content', 'vote')) {
            // Join on voting table
            $query->select('COALESCE(NULLIF(ROUND(v.rating_sum  / v.rating_count, 0), 0), 0) AS rating, 
							COALESCE(NULLIF(v.rating_count, 0), 0) as rating_count')
                    ->join('LEFT', '#__content_rating AS v ON a.id = v.content_id');
        }

        // Filter by access level.
        if ($access = $this->getState('filter.access')) {
            $groups = implode(',', $user->getAuthorisedViewLevels());
            $query->where('a.access IN (' . $groups . ')')
                    ->where('c.access IN (' . $groups . ')');
        }

        // Filter by published state
        $published = $this->getState('filter.published');

        if (is_numeric($published) && $published == 2) {
            // If category is archived then article has to be published or archived.
            // If categogy is published then article has to be archived.
            $query->where('((c.published = 2 AND a.state > 0) OR (c.published = 1 AND a.state = 2))');
        } elseif (is_numeric($published)) {
            // Category has to be published
            $query->where('c.published = 1 AND a.state = ' . (int) $published);
        } elseif (is_array($published)) {
            $published = ArrayHelper::toInteger($published);
            $published = implode(',', $published);

            // Category has to be published
            $query->where('c.published = 1 AND a.state IN (' . $published . ')');
        }

        // Filter by featured state
        $featured = $this->getState('filter.featured');

        switch ($featured) {
            case 'hide':
                $query->where('a.featured = 0');
                break;

            case 'only':
                $query->where('a.featured = 1');
                break;

            case 'show':
            default:
                // Normally we do not discriminate
                // between featured/unfeatured items.
                break;
        }

        // Filter by a single or group of articles.
        $articleId = $this->getState('filter.article_id');

        if (is_numeric($articleId)) {
            $type = $this->getState('filter.article_id.include', true) ? '= ' : '<> ';
            $query->where('a.id ' . $type . (int) $articleId);
        } elseif (is_array($articleId)) {
            $articleId = ArrayHelper::toInteger($articleId);
            $articleId = implode(',', $articleId);
            $type = $this->getState('filter.article_id.include', true) ? 'IN' : 'NOT IN';
            $query->where('a.id ' . $type . ' (' . $articleId . ')');
        }

        $categoryId = JRequest::getInt('id', 8);
        $query->where('a.catid =  ' . $categoryId);

        /*
          // Filter by a single or group of categories
          $categoryId = $this->getState('filter.category_id');

          if (is_numeric($categoryId)) {
          $type = $this->getState('filter.category_id.include', true) ? '= ' : '<> ';

          // Add subcategory check
          $includeSubcategories = $this->getState('filter.subcategories', false);
          $categoryEquals = 'a.catid ' . $type . (int) $categoryId;

          if ($includeSubcategories) {
          $levels = (int) $this->getState('filter.max_category_levels', '1');

          // Create a subquery for the subcategory list
          $subQuery = $db->getQuery(true)
          ->select('sub.id')
          ->from('#__categories as sub')
          ->join('INNER', '#__categories as this ON sub.lft > this.lft AND sub.rgt < this.rgt')
          ->where('this.id = ' . (int) $categoryId);

          if ($levels >= 0) {
          $subQuery->where('sub.level <= this.level + ' . $levels);
          }

          // Add the subquery to the main query
          $query->where('(' . $categoryEquals . ' OR a.catid IN (' . (string) $subQuery . '))');
          } else {
          $query->where($categoryEquals);
          }
          } elseif (is_array($categoryId) && (count($categoryId) > 0)) {
          $categoryId = ArrayHelper::toInteger($categoryId);
          $categoryId = implode(',', $categoryId);

          if (!empty($categoryId)) {
          $type = $this->getState('filter.category_id.include', true) ? 'IN' : 'NOT IN';
          $query->where('a.catid ' . $type . ' (' . $categoryId . ')');
          }
          }
         * */


        // Filter by author
        $authorId = $this->getState('filter.author_id');
        $authorWhere = '';

        if (is_numeric($authorId)) {
            $type = $this->getState('filter.author_id.include', true) ? '= ' : '<> ';
            $authorWhere = 'a.created_by ' . $type . (int) $authorId;
        } elseif (is_array($authorId)) {
            $authorId = ArrayHelper::toInteger($authorId);
            $authorId = implode(',', $authorId);

            if ($authorId) {
                $type = $this->getState('filter.author_id.include', true) ? 'IN' : 'NOT IN';
                $authorWhere = 'a.created_by ' . $type . ' (' . $authorId . ')';
            }
        }

        // Filter by author alias
        $authorAlias = $this->getState('filter.author_alias');
        $authorAliasWhere = '';

        if (is_string($authorAlias)) {
            $type = $this->getState('filter.author_alias.include', true) ? '= ' : '<> ';
            $authorAliasWhere = 'a.created_by_alias ' . $type . $db->quote($authorAlias);
        } elseif (is_array($authorAlias)) {
            $first = current($authorAlias);

            if (!empty($first)) {
                foreach ($authorAlias as $key => $alias) {
                    $authorAlias[$key] = $db->quote($alias);
                }

                $authorAlias = implode(',', $authorAlias);

                if ($authorAlias) {
                    $type = $this->getState('filter.author_alias.include', true) ? 'IN' : 'NOT IN';
                    $authorAliasWhere = 'a.created_by_alias ' . $type . ' (' . $authorAlias .
                            ')';
                }
            }
        }

        if (!empty($authorWhere) && !empty($authorAliasWhere)) {
            $query->where('(' . $authorWhere . ' OR ' . $authorAliasWhere . ')');
        } elseif (empty($authorWhere) && empty($authorAliasWhere)) {
            // If both are empty we don't want to add to the query
        } else {
            // One of these is empty, the other is not so we just add both
            $query->where($authorWhere . $authorAliasWhere);
        }

        // Define null and now dates
        $nullDate = $db->quote($db->getNullDate());
        $nowDate = $db->quote(JFactory::getDate()->toSql());

        // Filter by start and end dates.
        if ((!$user->authorise('core.edit.state', 'com_content')) && (!$user->authorise('core.edit', 'com_content'))) {
            $query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
                    ->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
        }

        // Filter by Date Range or Relative Date
        $dateFiltering = $this->getState('filter.date_filtering', 'off');
        $dateField = $this->getState('filter.date_field', 'a.created');

        switch ($dateFiltering) {
            case 'range':
                $startDateRange = $db->quote($this->getState('filter.start_date_range', $nullDate));
                $endDateRange = $db->quote($this->getState('filter.end_date_range', $nullDate));
                $query->where(
                        '(' . $dateField . ' >= ' . $startDateRange . ' AND ' . $dateField .
                        ' <= ' . $endDateRange . ')'
                );
                break;

            case 'relative':
                $relativeDate = (int) $this->getState('filter.relative_date', 0);
                $query->where(
                        $dateField . ' >= DATE_SUB(' . $nowDate . ', INTERVAL ' .
                        $relativeDate . ' DAY)'
                );
                break;

            case 'off':
            default:
                break;
        }

        // Process the filter for list views with user-entered filters
        if (is_object($params) && ($params->get('filter_field') !== 'hide') && ($filter = $this->getState('list.filter'))) {
            // Clean filter variable
            $filter = StringHelper::strtolower($filter);
            $hitsFilter = (int) $filter;
            $filter = $db->quote('%' . $db->escape($filter, true) . '%', false);

            switch ($params->get('filter_field')) {
                case 'author':
                    $query->where(
                            'LOWER( CASE WHEN a.created_by_alias > ' . $db->quote(' ') .
                            ' THEN a.created_by_alias ELSE ua.name END ) LIKE ' . $filter . ' '
                    );
                    break;

                case 'hits':
                    $query->where('a.hits >= ' . $hitsFilter . ' ');
                    break;

                case 'title':
                default:
                    // Default to 'title' if parameter is not valid
                    $query->where('LOWER( a.title ) LIKE ' . $filter);
                    break;
            }
        }

        // Filter by language
        if ($this->getState('filter.language')) {
            $query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
        }

        // Filter by a single or group of tags.
        $hasTag = false;
        $tagId = $this->getState('filter.tag');

        if (!empty($tagId) && is_numeric($tagId)) {
            $hasTag = true;

            $query->where($db->quoteName('tagmap.tag_id') . ' = ' . (int) $tagId);
        } elseif (is_array($tagId)) {
            ArrayHelper::toInteger($tagId);
            $tagId = implode(',', $tagId);
            if (!empty($tagId)) {
                $hasTag = true;

                $query->where($db->quoteName('tagmap.tag_id') . ' IN (' . $tagId . ')');
            }
        }

        if ($hasTag) {
            $query->join('LEFT', $db->quoteName('#__contentitem_tag_map', 'tagmap')
                    . ' ON ' . $db->quoteName('tagmap.content_item_id') . ' = ' . $db->quoteName('a.id')
                    . ' AND ' . $db->quoteName('tagmap.type_alias') . ' = ' . $db->quote('com_content.article')
            );
        }

        // Add the list ordering clause.
        $query->order('a.created DESC');

        return $query;
    }

    /**
     * Method to get an array of data items
     *
     * @return  mixed An array of data on success, false on failure.
     */
    public function getItems() {
        $items = parent::getItems();


        return $items;
    }

    /**
     * Overrides the default function to check Date fields format, identified by
     * "_dateformat" suffix, and erases the field if it's not correct.
     *
     * @return void
     */
    protected function loadFormData() {
        $app = Factory::getApplication();
        $filters = $app->getUserState($this->context . '.filter', array());
        $error_dateformat = false;

        foreach ($filters as $key => $value) {
            if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null) {
                $filters[$key] = '';
                $error_dateformat = true;
            }
        }

        if ($error_dateformat) {
            $app->enqueueMessage(JText::_("COM_EVENTS_SEARCH_FILTER_DATE_FORMAT"), "warning");
            $app->setUserState($this->context . '.filter', $filters);
        }

        return parent::loadFormData();
    }

    /**
     * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
     *
     * @param   string  $date  Date to be checked
     *
     * @return bool
     */
    private function isValidDate($date) {
        $date = str_replace('/', '-', $date);
        return (date_create($date)) ? Factory::getDate($date)->format("Y-m-d") : null;
    }

}
