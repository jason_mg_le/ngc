
/*
 * Created on : Jan 23, 2018, 9:19:30 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */


/*
 * Created on : Jan 23, 2018, 9:03:25 PM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    $('.list-events .tv-assign, .event-detail .tv-assign').click(function (e) {
        var $me = $(this);
        var $url = $(this).attr('action');
        if ($.trim($url) == '') {
            alert('Please login to Save To Calendar');
            return true;
        }
        $(".body").LoadingOverlay("show");
        $.ajax({
            url: $url,
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $(".body").LoadingOverlay("hide");
                if (data.result==true) {
                    $me.attr('disabled', '');
                    alert(data.html);
                } else {
                    alert(data.html);
                }
            }
        })
    })
});