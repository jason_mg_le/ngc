<?php

/**
 * @author  Xu Ding
 * @email   thedilab@gmail.com
 * @website http://www.StarTutorial.com
 * */
class Calendar {

    /**
     * Constructor
     */
    public function __construct() {
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);

        $next = 'Next';
        $prev = 'Prev';
    }

    /*     * ******************* PROPERTY ******************* */

    private $dayLabels = array("M", "T", "W", "T", "F", "S", "S");
    private $currentYear = 0;
    private $currentMonth = 0;
    private $currentDay = 0;
    private $currentDate = null;
    private $daysInMonth = 0;
    
    /*     * ******************* PUBLIC ********************* */

    /**
     * print out the calendar
     */
    public function show($month = 0, $year = 0, $arrDays = array(), $arrEvent = array()) {
        if ($year == 0) {
            $year = date("Y", time());
        }

        if ($month == 0) {

            $month = date("m", time());
        }

        $this->currentYear = $year;

        $this->currentMonth = $month;

        $this->daysInMonth = $this->_daysInMonth($month, $year);

        $content = '<div id="calendar">' .
                '<div class="box-header">' .
                $this->_createNavi() .
                '</div>' .
                '<div class="box-calendar">' .
                '<ul class="label">' . $this->_createLabels() . '</ul>';
        $content .= '<div class="clear"></div>';
        $content .= '<ul class="dates">';

        $weeksInMonth = $this->_weeksInMonth($month, $year);
        // Create weeks in a month
        for ($i = 0; $i < $weeksInMonth; $i++) {

            //Create days in a week
            for ($j = 1; $j <= 7; $j++) {
                $content .= $this->_showDay($i * 7 + $j, $arrDays, $arrEvent);
            }
        }

        $content .= '</ul>';

        $content .= '<div class="clear"></div>';

        $content .= '</div>';

        $content .= '</div>';
        return $content;
    }

    /*     * ******************* PRIVATE ********************* */

    /**
     * create the li element for ul
     */
    private function _showDay($cellNumber, $arrDays = array(), $arrEvent = array()) {

        if ($this->currentDay == 0) {

            $firstDayOfTheWeek = date('N', strtotime($this->currentYear . '-' . $this->currentMonth . '-01'));

            if (intval($cellNumber) == intval($firstDayOfTheWeek)) {

                $this->currentDay = 1;
            }
        }

        if (($this->currentDay != 0) && ($this->currentDay <= $this->daysInMonth)) {

            $this->currentDate = date('Y-m-d', strtotime($this->currentYear . '-' . $this->currentMonth . '-' . ($this->currentDay)));

            $cellContent = $this->currentDay > 9 ? $this->currentDay : '0' . $this->currentDay;

            $this->currentDay++;
        } else {

            $this->currentDate = null;

            $cellContent = null;
        }

        $class = '';
        $event = '';
        $textNumber = ''; // > 9 ? $cellNumber : '0' . $cellNumber;
        $currentDate = substr($this->currentDate, 8, 2);
        if (in_array($currentDate, $arrDays)) {
            $class = 'cala_day';
            $event = '<div class="tooltip_templates"><span id="tooltip_content' . $currentDate . '">' . $arrEvent[$currentDate] . '</span></div>';
            $textNumber = '<a  class="calan_tooltip" data-tooltip-content="#tooltip_content' . $currentDate . '" >' . $cellContent . '</a>';
        } else {
            $textNumber = $cellContent;
        }
        $today = '';
        if ($cellNumber == date('d') && date('m') == $this->currentMonth) {
            $today = 'today';
        }
        return '<li id="li-' . $this->currentDate . '" class="' . $class . ' ' . $today . ' ' . ($cellNumber % 7 == 1 ? ' start ' : ($cellNumber % 7 == 0 ? ' end ' : ' ')) .
                ($cellContent == null ? 'mask' : '') . '">' . $textNumber . $event . '</li>';
    }

    /**
     * create navigation
     */
    private function _createNavi() {

        $nextMonth = $this->currentMonth == 12 ? 1 : intval($this->currentMonth) + 1;

        $nextYear = $this->currentMonth == 12 ? intval($this->currentYear) + 1 : $this->currentYear;

        $preMonth = $this->currentMonth == 1 ? 12 : intval($this->currentMonth) - 1;

        $preYear = $this->currentMonth == 1 ? intval($this->currentYear) - 1 : $this->currentYear;

        return
                '<div class="header">' .
                '<a class="prev" href="' . $this->naviHref . '&date=' . $preYear . '-' . sprintf('%02d', $preMonth) . '">' . $this->prev . '</a>' .
                '<h4 class="title">' . date('F', strtotime($this->currentYear . '-' . $this->currentMonth . '-1')) . '</h4>' .
                '<a class="next" href="' . $this->naviHref . '&date=' . $nextYear . '-' . sprintf("%02d", $nextMonth) . '">' . $this->next . '</a>' .
                '</div>';
    }

    /**
     * create calendar week labels
     */
    private function _createLabels() {

        $content = '';

        foreach ($this->dayLabels as $index => $label) {

            $content .= '<li class="' . ($label == 6 ? 'end title' : 'start title') . ' title">' . $label . '</li>';
        }

        return $content;
    }

    /**
     * calculate number of weeks in a particular month
     */
    private function _weeksInMonth($month = null, $year = null) {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month, $year);

        $numOfweeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);

        $monthEndingDay = date('N', strtotime($year . '-' . $month . '-' . $daysInMonths));

        $monthStartDay = date('N', strtotime($year . '-' . $month . '-01'));

        if ($monthEndingDay < $monthStartDay) {

            $numOfweeks++;
        }

        return $numOfweeks;
    }

    /**
     * calculate number of days in a particular month
     */
    private function _daysInMonth($month = null, $year = null) {

        if (null == ($year))
            $year = date("Y", time());

        if (null == ($month))
            $month = date("m", time());

        return date('t', strtotime($year . '-' . $month . '-01'));
    }

}
