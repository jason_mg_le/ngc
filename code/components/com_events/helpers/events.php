<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Events
 * @author     Thang Tran <trantrongthang1207@gmail.com>
 * @copyright  2018 Thang Tran
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('EventsHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_events' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'events.php');

/**
 * Class EventsFrontendHelper
 *
 * @since  1.6
 */
class EventsHelpersEvents {

    /**
     * Get an instance of the named model
     *
     * @param   string  $name  Model name
     *
     * @return null|object
     */
    public static function getModel($name) {
        $model = null;

        // If the file exists, let's
        if (file_exists(JPATH_SITE . '/components/com_events/models/' . strtolower($name) . '.php')) {
            require_once JPATH_SITE . '/components/com_events/models/' . strtolower($name) . '.php';
            $model = JModelLegacy::getInstance($name, 'EventsModel');
        }

        return $model;
    }

    /**
     * Gets the files attached to an item
     *
     * @param   int     $pk     The item's id
     *
     * @param   string  $table  The table's name
     *
     * @param   string  $field  The field's name
     *
     * @return  array  The files
     */
    public static function getFiles($pk, $table, $field) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
                ->select($field)
                ->from($table)
                ->where('id = ' . (int) $pk);

        $db->setQuery($query);

        return explode(',', $db->loadResult());
    }

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item) {
        $permission = false;
        $user = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_events')) {
            $permission = true;
        } else {
            if (isset($item->created_by)) {
                if ($user->authorise('core.edit.own', 'com_events') && $item->created_by == $user->id) {
                    $permission = true;
                }
            } else {
                $permission = true;
            }
        }

        return $permission;
    }

    public function wordLimit($str, $limit = 100, $end_char = '&#8230;') {
        if (JString::trim($str) == '')
            return $str;

        // always strip tags for text
        $str = strip_tags($str);

        $find = array("/\r|\n/u", "/\t/u", "/\s\s+/u");
        $replace = array(" ", " ", " ");
        $str = preg_replace($find, $replace, $str);

        preg_match('/\s*(?:\S*\s*){' . (int) $limit . '}/u', $str, $matches);
        if (JString::strlen($matches[0]) == JString::strlen($str))
            $end_char = '';
        return JString::rtrim($matches[0]) . $end_char;
    }

    // Character limit
    public function characterLimit($str, $limit = 150, $end_char = '...') {
        if (JString::trim($str) == '')
            return $str;

        // always strip tags for text
        $str = strip_tags(JString::trim($str));

        $find = array("/\r|\n/u", "/\t/u", "/\s\s+/u");
        $replace = array(" ", " ", " ");
        $str = preg_replace($find, $replace, $str);

        if (JString::strlen($str) > $limit) {
            $str = JString::substr($str, 0, $limit);
            return JString::rtrim($str) . $end_char;
        } else {
            return $str;
        }
    }

    public static function add($post = array()) {
        include_once JPATH_COMPONENT . '/models/assign.php';
        $userAssign = JModelLegacy::getInstance('Assign', 'EventsModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        include_once JPATH_COMPONENT . '/models/assign.php';
        $userAssign = JModelLegacy::getInstance('Assign', 'EventsModel', array('ignore_request' => true));
        $objTable = $userAssign->getTable();
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete($id) {
        if ($id > 0) {
            include_once JPATH_COMPONENT . '/models/assign.php';
            $userAssign = JModelLegacy::getInstance('Assign', 'EventsModel', array('ignore_request' => true));
            $objTable = $userAssign->getTable();
            $objTable->delete($id);
        }
        return true;
    }

    public static function isUniquies($field = 'id', $value = 0, $table = '#__events_assign') {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
                ->select('*')
                ->from($db->quoteName($table))
                ->where($db->quoteName(trim($field)) . ' = ' . $db->quote(trim($value)));
        $db->setQuery($query);

        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return 0;
        }
    }

    public function isAssign($userId, $eventId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
                ->select('id')
                ->from($db->quoteName('#__events_assign'))
                ->where($db->quoteName('user_id') . ' = ' . $db->quote(trim($userId)))
                ->where($db->quoteName('event_id') . ' = ' . $db->quote(trim($eventId)));
        $db->setQuery($query);

        $objResult = $db->loadResult();
        if ($objResult) {
            return $objResult;
        } else {
            return 0;
        }
    }

    function formatDate($date, $format = 'd/m/Y') {
        if ($date == '0000-00-00' || $date == '' || $date === '0000-00-00 00:00:00') {
            return false;
        }
        if ($format == '' || $format == null) {
            $format = 'Y-m-d H:i:s';
        }
        $date = date_create($date);
        return date_format($date, $format);
    }

}
